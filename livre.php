<?php require_once 'includes/header.php'; ?>

<div class="row">

<?php 
	
	$sql = "SELECT biblio.nom_biblio, sum(livre.qte_livre) FROM livre
	INNER JOIN biblio ON livre.id_biblio = biblio.id_biblio
	 GROUP BY biblio.id_biblio";
	$result = $connect->query($sql);

	while($row = $result->fetch_array()){

		echo "<div class='col-md-3'>
		<div class='panel panel-warning'>
			<div class='panel-heading'>
					<b>$row[0]</b>
					<span class='badge pull pull-right'> $row[1] </span>
				
			</div> <!--/panel-hdeaing-->
		</div> <!--/panel-->
	</div> <!--/col-md-4-->";

	}
//}	

$connect->close();
?>
	<div class="col-md-12">

		<ol class="breadcrumb">
		  <li><a href="dashboard.php">Accueil</a></li>		  
		  <li class="active">Livre</li>
		</ol>

		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="page-heading"> <i class="glyphicon glyphicon-edit"></i> Gestion des livres</div>
			</div> <!-- /panel-heading -->
			<div class="panel-body">

				<div class="remove-messages"></div>			
				
				<table class="table" id="manageLivreTable">
					<thead>
						<tr>							
							<th>Image</th>
							<th>Titre</th>
							<th>Editeur</th>
							<th>Genre</th>
							<th>Langue</th>
							<th>Quantité</th>
							<th>Résumer</th>
							<th>Bibliothèque</th>
						</tr>
					</thead>
				</table>
				<!-- /table -->

			</div> <!-- /panel-body -->
		</div> <!-- /panel -->		
	</div> <!-- /col-md-12 -->
</div> <!-- /row -->


<script src="custom/js/livre.js"></script>

<?php require_once 'includes/footer.php'; ?>