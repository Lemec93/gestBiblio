<?php require_once 'php_action/core.php';

//$result = $connect->query($sql);


?>


<!DOCTYPE html>
<html>
<head>

	<title>Gestion Bibliothèque</title>

	<!-- bootstrap -->
	<link rel="stylesheet" href="assests/bootstrap/css/bootstrap.min.css">
	<!-- bootstrap theme-->
	<link rel="stylesheet" href="assests/bootstrap/css/bootstrap-theme.min.css">
	<!-- font awesome -->
	<link rel="stylesheet" href="assests/font-awesome/css/font-awesome.min.css">

  <!-- custom css -->
  <link rel="stylesheet" href="custom/css/custom.css">
  <link rel="stylesheet" href="custom/css/affStyle.css">


	<!-- DataTables -->
  <link rel="stylesheet" href="assests/plugins/datatables/jquery.dataTables.min.css">

  <!-- file input -->
  <link rel="stylesheet" href="assests/plugins/fileinput/css/fileinput.min.css">

  <!-- jquery -->
	<script src="assests/jquery/jquery.min.js"></script>
  <!-- jquery ui -->  
  <link rel="stylesheet" href="assests/jquery-ui/jquery-ui.min.css">
  <script src="assests/jquery-ui/jquery-ui.min.js"></script>

  <!-- bootstrap js -->
	<script src="assests/bootstrap/js/bootstrap.min.js"></script>

</head>
<body>
<!--?php 
$sql = "SELECT 'nomuser','prenuser' FROM users WHERE user_id = 'userId'";
$result = $connect->query($sql);
//echo "$result";


?-->


	<nav class="navbar navbar-default navbar-static-top">
		<div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!-- <a class="navbar-brand" href="#">Brand</a> -->
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">      

      <ul class="nav navbar-nav navbar-right">        

      	<li id="navDashboardUser"><a href="index.php"><i class="glyphicon glyphicon-list-alt"></i>  Tableau de bord</a></li>        
                

        <li id="navLivreUser"><a href="livreUser.php"> <i class="glyphicon glyphicon-book"></i> Livre</a></li>          

        <li id="navEmpr"><a href="emprUser.php"><i class="glyphicon glyphicon-btc"></i>  Emprunt</a></li>

        <li class="dropdown" id="navSettingUser">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="glyphicon glyphicon-user"></i><span class="caret"></span></a>
          <ul class="dropdown-menu">            
            <li id="topNavSettingUser"><a href="settingUser.php"> <i class="glyphicon glyphicon-wrench"></i> Option</a></li>            
            <li id="topNavLogout"><a href="deconnexion.php"> <i class="glyphicon glyphicon-log-out"></i> Déconnoxion</a></li>            
          </ul>
        </li>        
               
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
	</nav>

	<div class="container">