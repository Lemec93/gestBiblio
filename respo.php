<?php require_once 'php_action/db_connect.php' ?>
<?php require_once 'includes/header.php'; ?>

<div class="row">
	<div class="col-md-12">

		<ol class="breadcrumb">
		  <li><a href="dashboard.php">Accueil</a></li>		  
		  <li class="active">Responsable</li>
		</ol>

		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="page-heading"> <i class="glyphicon glyphicon-edit"></i> Gestion resposanble</div>
			</div> <!-- /panel-heading -->
			<div class="panel-body">

				<div class="remove-messages"></div> 

				<div class="div-action pull pull-right" style="padding-bottom:20px;">
					<button class="btn btn-default button1" data-toggle="modal" id="addRespoModalBtn" data-target="#addRespoModal"> <i class="glyphicon glyphicon-plus-sign"></i> Ajouter responsable </button>
				</div> <!-- /div-action -->				
				
				<table class="table" id="manageRespoTable">
					<thead>
						<tr>
							<th style="width:10%;">Photo</th>							
							<th>Nom</th>
							<th>Prénoms</th>							
							<th>Contact</th>
							<th>Email</th>
							<th>Mot de passe</th>
							<th>Bibliothèque</th>
							<th style="width:15%;">Options</th>
						</tr>
					</thead>
				</table>
				<!-- /table -->

			</div> <!-- /panel-body -->
		</div> <!-- /panel -->		
	</div> <!-- /col-md-12 -->
</div> <!-- /row -->


<!-- add product -->
<div class="modal fade" id="addRespoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">

    	<form class="form-horizontal" id="submitRespoForm" action="php_action/createRespo.php" method="POST" enctype="multipart/form-data">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-plus"></i> Ajouter responsable</h4>
	      </div>

	      <div class="modal-body" style="max-height:450px; overflow:auto;">

	      	<div id="add-respo-messages"></div>
     	           	       

	        <div class="form-group">
	        	<label for="nom_respo" class="col-sm-3 control-label">Nom: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="nom_respo" placeholder="Nom" name="nom_respo" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->

	        <div class="form-group">
	        	<label for="pre_respo" class="col-sm-3 control-label">Prénoms: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="pre_respo" placeholder="Prénoms" name="pre_respo" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->

	        <div class="form-group">
	        	<label for="tel_respo" class="col-sm-3 control-label">Contact: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="tel_respo" placeholder="Ex: 07070720" name="tel_respo" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->

	        

			<div class="form-group">
	        	<label for="eml_respo" class="col-sm-3 control-label">Email: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="email" class="form-control" id="eml_respo" placeholder="Ex: xxx@email.com" name="eml_respo" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->	        	   	        				        	         	       

	        <div class="form-group">
	        	<label for="slt_biblio" class="col-sm-3 control-label">Selection bibliothèque: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <select type="text" class="form-control" id="slt_biblio" placeholder="Liste bibliothèque" name="stl_biblio" >
				      	<option value="">~~SELECT~~</option>
				      	<?php 
				      	$sql = "SELECT id_biblio, nom_biblio, vil_biblio, act_biblio, sta_biblio FROM biblio WHERE sta_biblio = 1 AND act_biblio = 1";
								$result = $connect->query($sql);

								while($row = $result->fetch_array()) {
									echo "<option value='".$row[0]."'>".$row[1]."</option>";
								} // while
								
				      	?>
				      </select>
				    </div>
	        </div> <!-- /form-group-->
	        	         	        
	      </div> <!-- /modal-body --> 
	      
		<div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Annuler</button>
	        
	        <button type="submit" class="btn btn-primary" id="createRespoBtn" data-loading-text="Loading..." autocomplete="off"> <i class="glyphicon glyphicon-ok-sign"></i> Enrégistrer</button>
	      </div> <!-- /modal-footer -->      
     	</form> <!-- /.form -->	     
    </div> <!-- /modal-content -->    
  </div> <!-- /modal-dailog -->
</div> 
<!-- /add categories -->


<!-- edit categories brand -->
<div class="modal fade" id="editRespoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    	    	
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-edit"></i> Modifier</h4>
	      </div>
	      <div class="modal-body" style="max-height:450px; overflow:auto;">

	      	<div class="div-loading">
	      		<!--i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i-->
						<span class="sr-only">Chargement...</span>
	      	</div>

	      	<div class="div-result">

				  <div class="tab-content">

				  	
				    <
				    	<form class="form-horizontal" id="editRespoForm" action="php_action/editRespo.php" method="POST">				    
				    	<br />

				    	<div id="edit-respo-messages"></div>


				    	<div class="form-group">
			        	<label for="editNomRespo" class="col-sm-3 control-label">Nom: </label>
			        	<label class="col-sm-1 control-label">: </label>
						    <div class="col-sm-8">
						      <input type="text" class="form-control" id="editNomRespo" placeholder="Nom" name="editNomRespo" autocomplete="off">
						    </div>
			        </div> <!-- /form-group-->	    

			        <div class="form-group">
			        	<label for="editPreRespo" class="col-sm-3 control-label">Prénoms: </label>
			        	<label class="col-sm-1 control-label">: </label>
						    <div class="col-sm-8">
						      <input type="text" class="form-control" id="editPreRespo" placeholder="Prénoms" name="editPreRespo" autocomplete="off">
						    </div>
			        </div> <!-- /form-group-->	        	 

			        <div class="form-group">
			        	<label for="editTelRespo" class="col-sm-3 control-label">Contact: </label>
			        	<label class="col-sm-1 control-label">: </label>
						    <div class="col-sm-8">
						      <input type="text" class="form-control" id="editTelRespo" placeholder="Contact" name="editTelRespo" autocomplete="off">
						    </div>
			        </div> <!-- /form-group-->

			        <div class="form-group">
	        	<label for="editEmlRespo" class="col-sm-3 control-label">Email: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="email" class="form-control" id="editEmlRespo" placeholder="Ex: xxx@email.com" name="editEmlRespo" autocomplete="off">
				    </div>
	        </div> 	     	

	        <div class="form-group">
	        	<label for="editPwdRespo" class="col-sm-3 control-label">Mot de passe: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="editPwdRespo" placeholder="***********" name="editPwdRespo" autocomplete="off">
				    </div>
	        </div>         

			        <div class="form-group">
			        	<label for="editSltBiblio" class="col-sm-3 control-label">Bibliothèque: </label>
			        	<label class="col-sm-1 control-label">: </label>
						    <div class="col-sm-8">
						      <select class="form-control" id="editSltBiblio" name="editSltBiblio">
						      	<option value="">~~SELECT~~</option>
						      	<?php 
						      	$sql = "SELECT id_biblio, nom_biblio, vil_biblio, act_biblio, sta_biblio FROM biblio WHERE sta_biblio = 1 AND act_biblio = 1";
								$result = $connect->query($sql);

										while($row = $result->fetch_array()) {
											echo "<option value='".$row[0]."'>".$row[1]."</option>";
										} // while
										
						      	?>
						      </select>
						    </div>
			        </div> <!-- /form-group editRespoFooter  -->					        	         	       
         	        

			        <div class="modal-footer editRespoFooter">
				        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Annuler</button>
				        
				        <button type="submit" class="btn btn-success" id="editRespoBtn" data-loading-text="Loading..."> <i class="glyphicon glyphicon-ok-sign"></i> Enrégistrer</button>
				      </div> <!-- /modal-footer -->				     
			        </form> <!-- /.form -->
				  </div>

				</div>
	      	
	      </div> <!-- /modal-body -->
	      	      
     	
    </div>
    <!-- /modal-content -->
  </div>
  <!-- /modal-dailog -->
</div>
<!-- /categories brand -->

<!-- categories brand -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeRespoModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="glyphicon glyphicon-trash"></i> Supprimer</h4>
      </div>
      <div class="modal-body">

      	<div class="removeRespoMessages"></div>

        <p>Voulez-vous vraiment supprimer?</p>
      </div>
      <div class="modal-footer removeRespoFooter">
        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Annuler</button>
        <button type="button" class="btn btn-primary" id="removeRespoBtn" data-loading-text="Loading..."> <i class="glyphicon glyphicon-ok-sign"></i> Supprimer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /categories brand -->


<script src="custom/js/respo.js"></script>

<?php require_once 'includes/footer.php'; ?>