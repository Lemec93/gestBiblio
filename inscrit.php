<?php require_once 'includes/header.php'; ?>

<div class="row">

<?php 
	
	$sql = "SELECT biblio.nom_biblio, COUNT(user.id_user) FROM user
	INNER JOIN biblio ON user.id_biblio = biblio.id_biblio
	 WHERE user.id_fonc = 3 OR user.id_fonc = 4
	 GROUP BY biblio.id_biblio";
	$result = $connect->query($sql);

	while($row = $result->fetch_array()){

		echo "<div class='col-md-3'>
		<div class='panel panel-warning'>
			<div class='panel-heading'>
					<b>$row[0]</b>
					<span class='badge pull pull-right'> $row[1] </span>
				
			</div> <!--/panel-hdeaing-->
		</div> <!--/panel-->
	</div> <!--/col-md-4-->";

	}
//}	

$connect->close();
?>

	<div class="col-md-12">

		<ol class="breadcrumb">
		  <li><a href="dashboard.php">Accueil</a></li>		  
		  <li class="active">Inscrit</li>
		</ol>

		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="page-heading"> <i class="glyphicon glyphicon-edit"></i> Liste des inscrits</div>
			</div> <!-- /panel-heading -->
			<div class="panel-body">

				<div class="remove-messages"></div>

				<table class="table" id="manageInscritTable">
					<thead>
						<tr>
							<th style="width:10%;">Photo</th>							
							<th>Nom</th>
							<th>Prénoms</th>							
							<th>Contact</th>
							<th>Email</th>
							<th>Mot de passe</th>
							<th>Fonction</th>
							<th>Bibliothèque</th>
						</tr>
					</thead>
				</table>
				<!-- /table -->

			</div> <!-- /panel-body -->
		</div> <!-- /panel -->		
	</div> <!-- /col-md-12 -->
</div> <!-- /row -->


<script src="custom/js/inscrit.js"></script>

<?php require_once 'includes/footer.php'; ?>