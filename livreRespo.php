<?php require_once 'includes/headerRespo.php'; ?>


<div class="row">
	<div class="col-md-12">

		<ol class="breadcrumb">
		  <li><a href="dashboard.php">Accueil</a></li>		  
		  <li class="active">Livre</li>
		</ol>

		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="page-heading"> <i class="glyphicon glyphicon-edit"></i> Gestion des livres</div>
			</div> <!-- /panel-heading -->
			<div class="panel-body">

				<div class="remove-messages"></div>

				<div class="div-action pull pull-right" style="padding-bottom:20px;">
					<button class="btn btn-default button1" data-toggle="modal" data-target="#addLivreModel" id="addLivreModalBtn"> <i class="glyphicon glyphicon-plus-sign"></i> Ajouter livre </button>
				</div> <!-- /div-action -->				
				
				<table class="table" id="manageLivreRespoTable">
					<thead>
						<tr>							
							<th>Image</th>
							<th>Titre</th>
							<th>Auteur</th>
							<th>Editeur</th>
							<th>Genre</th>
							<th>Langue</th>
							<th>Quantité</th>
							<th>Status</th>
							<th style="width:15%;">Options</th>
						</tr>
					</thead>
				</table>
				<!-- /table -->

			</div> <!-- /panel-body -->
		</div> <!-- /panel -->		
	</div> <!-- /col-md-12 -->
</div> <!-- /row -->

<div class="modal fade" id="addLivreModel" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    	
    	<form class="form-horizontal" id="submitLivreForm" action="php_action/createLivre.php" method="POST" enctype="multipart/form-data">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-plus"></i> Ajouter livre</h4>
	      </div>
	      <div class="modal-body" style="max-height:450px; overflow:auto;">

	      	<div id="add-livre-messages"></div>

	        <div class="form-group">
	        	<label for="titLivre" class="col-sm-3 control-label">Titre: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="titLivre" placeholder="Entrez le titre" name="titLivre" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->

	        <div class="form-group">
	        	<label for="ediLivre" class="col-sm-3 control-label">Editeur: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="ediLivre" placeholder="Editeur" name="ediLivre" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->

	        <div class="form-group">
	        	<label for="autLivre" class="col-sm-3 control-label">Auteur: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="autLivre" placeholder="Auteur" name="autLivre" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->	

	        <div class="form-group">
	        	<label for="genLivre" class="col-sm-3 control-label">Genre: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="genLivre" placeholder="Entrez le genre" name="genLivre" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->

	        <div class="form-group">
	        	<label for="lanLivre" class="col-sm-3 control-label">Langue: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="lanLivre" placeholder="Langue" name="lanLivre" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->

	        <div class="form-group">
	        	<label for="qteLivre" class="col-sm-3 control-label">Quantité: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="qteLivre" placeholder="Quantité" name="qteLivre" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->

	        <div class="form-group">
	        	<label for="resLivre" class="col-sm-3 control-label">Resumé: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <textarea id="resLivre" class="form-control" name="resLivre" autocomplete="off"></textarea>
				    </div>
	        </div> <!-- /form-group-->		

	        <div class="form-group">
	        	<label for="staLivre" class="col-sm-3 control-label">Status: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <select class="form-control" id="staLivre" name="staLivre">
				      	<option value="">~~SELECT~~</option>
				      	<option value="1">Disponible</option>
				      	<option value="2">Indisponible</option>
				      </select>
				    </div>
	        </div> <!-- /form-group-->	         	        

	      </div> <!-- /modal-body -->
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
	        
	        <button type="submit" class="btn btn-primary" id="createLivreBtn" data-loading-text="Loading..." autocomplete="off">Enrégistrer</button>
	      </div>
	      <!-- /modal-footer -->
     	</form>

	     <!-- /.form -->
    </div>
    <!-- /modal-content -->
  </div>
  <!-- /modal-dailog -->
</div>
<!-- / add modal -->

<!-- edit biblio -->
<div class="modal fade" id="editLivreRespoModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    	    	
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-edit"></i> Modifier</h4>
	      </div>
	      <div class="modal-body" style="max-height:450px; overflow:auto;">

	      	<div class="div-loading">
	      		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
						<span class="sr-only">Changement...</span>
	      	</div>

	      	<div class="div-result">

				  <!-- Tab panes -->
				  <div class="tab-content">

			
				    	<form class="form-horizontal" id="editLivreRespoForm" action="php_action/editLivreRespo.php" method="POST">				    
				    	<br />

				    	<div id="edit-livreRespo-messages"></div>

				    	<div class="form-group">
			        	<label for="editTitLivre" class="col-sm-3 control-label">Titre livre: </label>
			        	<label class="col-sm-1 control-label">: </label>
						    <div class="col-sm-8">
						      <input type="text" class="form-control" id="editTitLivre" placeholder="Titre livre" name="editTitLivre" autocomplete="off">
						    </div>
			        </div> <!-- /form-group-->	    

			        <div class="form-group">
			        	<label for="editEdiLivre" class="col-sm-3 control-label">Editeur: </label>
			        	<label class="col-sm-1 control-label">: </label>
						    <div class="col-sm-8">
						      <input type="text" class="form-control" id="editEdiLivre" placeholder="Editeur" name="editEdiLivre" autocomplete="off">
						    </div>
			        </div> <!-- /form-group-->	    

			        <div class="form-group">
			        	<label for="editAutLivre" class="col-sm-3 control-label">Auteur: </label>
			        	<label class="col-sm-1 control-label">: </label>
						    <div class="col-sm-8">
						      <input type="text" class="form-control" id="editAutLivre" placeholder="Auteur" name="editAutLivre" autocomplete="off">
						    </div>
			        </div> <!-- /form-group-->	        	 

			        <div class="form-group">
			        	<label for="editGenLivre" class="col-sm-3 control-label">Genre: </label>
			        	<label class="col-sm-1 control-label">: </label>
						    <div class="col-sm-8">
						      <input type="text" class="form-control" id="editGenLivre" placeholder="Genre" name="editGenLivre" autocomplete="off">
						    </div>
			        </div> <!-- /form-group-->	     	        

			        <div class="form-group">
			        	<label for="editLanLivre" class="col-sm-3 control-label">Langue: </label>
			        	<label class="col-sm-1 control-label">: </label>
						    <div class="col-sm-8">
						      <input type="text" class="form-control" id="editLanLivre" placeholder="Langue" name="editLanLivre" autocomplete="off">
						    </div>
			        </div> <!-- /form-group-->		

			        <div class="form-group">
			        	<label for="editQteLivre" class="col-sm-3 control-label">Quantité: </label>
			        	<label class="col-sm-1 control-label">: </label>
						    <div class="col-sm-8">
						      <input type="text" class="form-control" id="editQteLivre" placeholder="Quantité" name="editQteLivre" autocomplete="off">
						    </div>
			        </div> <!-- /form-group-->

			        <div class="form-group">
	        			<label for="editResLivre" class="col-sm-3 control-label">Resumé: </label>
	        			<label class="col-sm-1 control-label">: </label>
					    <div class="col-sm-8">
				   		   <textarea id="editResLivre" class="form-control" name="editResLivre" autocomplete="off"></textarea>
				  	    </div>
	      		    </div> <!-- /form-group-->					        	         	       

			        <div class="form-group">
			        	<label for="editStaLivre" class="col-sm-3 control-label">Status: </label>
			        	<label class="col-sm-1 control-label">: </label>
						    <div class="col-sm-8">
						      <select class="form-control" id="editStaLivre" name="editStaLivre">
						      	<option value="">~~SELECT~~</option>
						      	<option value="1">Disponible</option>
						      	<option value="2">Indisponible</option>
						      </select>
						    </div>
			        </div> <!-- /form-group-->	         	        

			        <div class="modal-footer editLivreRespoFooter">
				        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Fermer</button>
				        
				        <button type="submit" class="btn btn-success" id="editLivreRespoBtn" data-loading-text="Loading..."> <i class="glyphicon glyphicon-ok-sign"></i> Modifier</button>
				      </div> <!-- /modal-footer -->				     
			        </form> <!-- /.form -->				     	
				     
				    <!-- /product info -->
				  </div>

				</div>
	      	
	      </div> <!-- /modal-body -->
	      	      
     	
    </div>
    <!-- /modal-content -->
  </div>
  <!-- /modal-dailog -->
</div>


>

<!-- remove brand -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeLivreRespoModel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="glyphicon glyphicon-trash"></i> Supprimer Livre</h4>
      </div>
      <div class="modal-body">

      	<div class="removeLivreRespoMessages"></div>
        
        <p>Voulez-vous vraiment supprimer?</p>
      </div>
      <div class="modal-footer removeLivreFooter">
        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Annuler</button>
        <button type="button" class="btn btn-primary" id="removeLivreBtn" data-loading-text="Loading..."> <i class="glyphicon glyphicon-ok-sign"></i> Supprimer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /remove brand -->

<script src="custom/js/livreRespo.js"></script>

<?php require_once 'includes/footer.php'; ?>