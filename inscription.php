<?php require_once 'php_action/db_connect.php' ?>
<?php require_once 'includes/headerRespo.php'; ?>

<div class="row">
	<div class="col-md-12">

		<ol class="breadcrumb">
		  <li><a href="dashboardRespo.php">Accueil</a></li>		  
		  <li class="active">Inscription</li>
		</ol>

		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="page-heading"> <i class="glyphicon glyphicon-edit"></i> Gestion des inscrits</div>
			</div> <!-- /panel-heading -->
			<div class="panel-body">

				<div class="remove-messages"></div>

				<div class="div-action pull pull-right" style="padding-bottom:20px;">
					<button class="btn btn-default button1" data-toggle="modal" id="addInscModalBtn" data-target="#addInscModal"> <i class="glyphicon glyphicon-plus-sign"></i> Inscription</button>
				</div> <!-- /div-action -->				
				
				<table class="table" id="manageInscTable">
					<thead>
						<tr>
							<th style="width:10%;">Photo</th>							
							<th>Nom</th>
							<th>Prénoms</th>
							<th>Detail</th>							
							<th>Contact</th>
							<th>Email</th>
							<th>Mot de passe</th>
							<th>Fonction</th>
							<th style="width:15%;">Options</th>
						</tr>
					</thead>
				</table>
				<!-- /table -->

			</div> <!-- /panel-body -->
		</div> <!-- /panel -->		
	</div> <!-- /col-md-12 -->
</div> <!-- /row -->


<!-- add product -->
<div class="modal fade" id="addInscModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">

    	<form class="form-horizontal" id="submitInscForm" action="php_action/createInsc.php" method="POST" enctype="multipart/form-data">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-plus"></i> Inscription</h4>
	      </div>

	      <div class="modal-body" style="max-height:450px; overflow:auto;">

	      	<div id="add-insc-messages"></div>
     	           	       

	        <div class="form-group">
	        	<label for="nom_insc" class="col-sm-3 control-label">Nom: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="nom_insc" placeholder="Nom" name="nom_insc" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->

	        <div class="form-group">
	        	<label for="pre_insc" class="col-sm-3 control-label">Prénoms: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="pre_insc" placeholder="Prénoms" name="pre_insc" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->

	        <div class="form-group">
	        	<label for="tel_insc" class="col-sm-3 control-label">Contact: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="tel_insc" placeholder="Ex: 07070720" name="tel_insc" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->

	        

			<div class="form-group">
	        	<label for="eml_insc" class="col-sm-3 control-label">Email: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="email" class="form-control" id="eml_insc" placeholder="Ex: xxx@email.com" name="eml_insc" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->	        	   	        				        	         	       
	        <div class="form-group">
	        	<label for="slt_fonc" class="col-sm-3 control-label">Fonction: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <select type="text" class="form-control" id="slt_fonc" placeholder="Fonction" name="slt_fonc" >
				      	<option value="">~~SELECT~~</option>
				      	<?php 
				      	$sql = "SELECT id_fonc, lib_fonc FROM fonction WHERE id_fonc = 3 OR id_fonc = 4";
								$result = $connect->query($sql);

								while($row = $result->fetch_array()) {
									echo "<option value='".$row[0]."'>".$row[1]."</option>";
								} // while
								
				      	?>
				      </select>
				    </div>
	        </div> <!-- /form-group-->

	        <div class="form-group">
	        	<label for="det_insc" class="col-sm-3 control-label">Detail Inscrit: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="det_insc" placeholder="Ex: LP/SI OU Prof de Informatique" name="det_insc" autocomplete="off">
				    </div>
	        </div>
	        	         	        
	      </div> <!-- /modal-body -->
	      
		<div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Annuler</button>
	        
	        <button type="submit" class="btn btn-primary" id="createInscBtn" data-loading-text="Loading..." autocomplete="off"> <i class="glyphicon glyphicon-ok-sign"></i> Enrégistrer</button>
	      </div> <!-- /modal-footer -->      
     	</form> <!-- /.form -->	     
    </div> <!-- /modal-content -->    
  </div> <!-- /modal-dailog -->
</div> 
<!-- /add categories -->


<!-- edit categories brand -->
<div class="modal fade" id="editInscModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    	    	
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-edit"></i> Modifier</h4>
	      </div>
	      <div class="modal-body" style="max-height:450px; overflow:auto;">

	      	<div class="div-loading">
	      		<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
						<span class="sr-only">Chargement...</span>
	      	</div>

	      	<div class="div-result">

				  <!-- Tab panes -->
				  <div class="tab-content">

				  	
				    	<form class="form-horizontal" id="editInscForm" action="php_action/editInsc.php" method="POST">				    
				    	<br />

				    	<div id="edit-insc-messages"></div>

				    	<div class="form-group">
			        	<label for="editNomInsc" class="col-sm-3 control-label">Nom: </label>
			        	<label class="col-sm-1 control-label">: </label>
						    <div class="col-sm-8">
						      <input type="text" class="form-control" id="editNomInsc" placeholder="Nom" name="editNomInsc" autocomplete="off">
						    </div>
			        </div> <!-- /form-group-->	    

			        <div class="form-group">
			        	<label for="editPreInsc" class="col-sm-3 control-label">Prénoms: </label>
			        	<label class="col-sm-1 control-label">: </label>
						    <div class="col-sm-8">
						      <input type="text" class="form-control" id="editPreInsc" placeholder="Prénoms" name="editPreInsc" autocomplete="off">
						    </div>
			        </div> <!-- /form-group-->

	        <div class="form-group">
	        	<label for="editTelInsc" class="col-sm-3 control-label">Contact: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="editTelInsc" placeholder="Ex: 07070720" name="editTelInsc" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->

	        

			<div class="form-group">
	        	<label for="editEmlInsc" class="col-sm-3 control-label">Email: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="email" class="form-control" id="editEmlInsc" placeholder="Ex: xxx@email.com" name="editEmlInsc" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->


			<div class="form-group">
	        	<label for="editPwdInsc" class="col-sm-3 control-label">Mot de passe: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="editPwdInsc" placeholder="Mot de passe" name="editPwdInsc" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->	        	   	        				        	         	       	        	 

			         <div class="form-group">
	        	<label for="editSltFonc" class="col-sm-3 control-label">Fonction: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <select type="text" class="form-control" id="editSltFonc" placeholder="Fonction" name="editSltFonc" >
				      	<option value="">~~SELECT~~</option>
				      	<?php 
				      	$sql = "SELECT id_fonc, lib_fonc FROM fonction WHERE id_fonc = 3 OR id_fonc = 4";
								$result = $connect->query($sql);

								while($row = $result->fetch_array()) {
									echo "<option value='".$row[0]."'>".$row[1]."</option>";
								} // while
								
				      	?>
				      </select>
				    </div>
	        </div> <!-- /form-group-->

	        <div class="form-group">
	        	<label for="editDetInsc" class="col-sm-3 control-label">Detail Inscrit: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="editDetInsc" placeholder="Ex: LP/SI OU Prof de Informatique" name="editDetInsc" autocomplete="off">
				    </div>
	        </div>   	         	       
         	        

			        <div class="modal-footer editInscFooter">
				        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Annuler</button>
				        
				        <button type="submit" class="btn btn-success" id="editInscBtn" data-loading-text="Loading..."> <i class="glyphicon glyphicon-ok-sign"></i> Enrégistrer</button>
				      </div> <!-- /modal-footer -->				     
			        </form> <!-- /.form -->				     	
				    
				    <!-- /product info -->
				  </div>

				</div>
	      	
	      </div> <!-- /modal-body -->
	      	      
     	
    </div>
    <!-- /modal-content -->
  </div>
  <!-- /modal-dailog -->
</div>
<!-- /categories brand -->

<!-- categories brand -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeInscModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="glyphicon glyphicon-trash"></i> Supprimer</h4>
      </div>
      <div class="modal-body">

      	<div class="removeInscMessages"></div>

        <p>Voulez-vous vraiment supprimer?</p>
      </div>
      <div class="modal-footer removeInscFooter">
        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Annuler</button>
        <button type="button" class="btn btn-primary" id="removeInscBtn" data-loading-text="Loading..."> <i class="glyphicon glyphicon-ok-sign"></i> Supprimer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /categories brand -->


<script src="custom/js/inscription.js"></script>

<?php require_once 'includes/footer.php'; ?>