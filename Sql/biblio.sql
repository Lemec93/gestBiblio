CREATE  database gestBiblio;

USE gestBiblio;


--
-- Table structure for table `bibliothèque`
--

CREATE TABLE IF NOT EXISTS `biblio` (
  `id_biblio` int(3) NOT NULL AUTO_INCREMENT,
  `nom_biblio` varchar(255) NOT NULL,
  `vil_biblio` varchar(255) NOT NULL,
  `act_biblio` int(1) NOT NULL DEFAULT '0',
  `sta_biblio` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_biblio`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bibliothèque`
--

INSERT INTO `biblio` (`id_biblio`, `nom_biblio`, `vil_biblio`, `act_biblio`, `sta_biblio`) VALUES
(1, 'BIBLIOTHEQUE GENERALE','Centrale', 1, 2);


--
-- Table structure for table `livre`
--

CREATE TABLE IF NOT EXISTS `livre` (
  `id_livre` int(11) NOT NULL AUTO_INCREMENT,
  `tit_livre` varchar(255) NOT NULL,
  `aut_livre` varchar(255) NOT NULL,
  `edi_livre` varchar(255) NOT NULL,
  `gen_livre` varchar(255) NOT NULL,
  `lan_livre` varchar(255) NOT NULL,
  `qte_livre` int(11) NOT NULL,
  `res_livre` text NOT NULL,
  `img_livre` text NOT NULL,
  `act_livre` int(11) NOT NULL DEFAULT '0',
  `sta_livre` int(11) NOT NULL DEFAULT '0',
  `id_biblio` int(11) NOT NULL,
  PRIMARY KEY (`id_livre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `livre`
--
/*
INSERT INTO `livre` (`id_livre`, `tit_livre`, `edi_livre`, `gen_livre`, `lan_livre`, `qte_livre`, `res_livre`, `img_livre`, `act_livre`, `sta_livre`) VALUES
(1, 'Livre A','Editeur A','Genre A','Francais',10,'BlablablaBlablablaBlablabla BlablablaBlablablaBlablabla BlablablaBlablablaBlablabla', '../assests/images/livre.jpg', 1, 1);
*/




--
-- Table structure for table `fonction`
--

CREATE TABLE IF NOT EXISTS `fonction` (
  `id_fonc` int(11) NOT NULL AUTO_INCREMENT,
  `lib_fonc` varchar(255) NOT NULL,
  PRIMARY KEY (`id_fonc`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fonction`
--

INSERT INTO `fonction` (`id_fonc`,`lib_fonc`) VALUES
(1,'Administrateur'),(2,'Responsable'),(3,'Professeur'),(4,'Etudiant');



--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nom_user` varchar(255) NOT NULL,
  `pre_user` varchar(255) NOT NULL,
  `tel_user` varchar(255) NOT NULL,
  `eml_user` varchar(255) NOT NULL,
  `pwd_user` varchar(255) NOT NULL,
  `pho_user` text NOT NULL,
  `det_user` varchar(255) NOT NULL,
  `id_fonc` int(11) NOT NULL,
  `id_biblio` int(11) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`,`nom_user`,`pre_user`,`tel_user`,`eml_user`,`pwd_user`,`pho_user`,`det_user`,`id_fonc`,`id_biblio`) VALUES
(1,'DJAMA','Med', '47362864','admin', '0', '../assests/images/inconnu.jpg','Administrateur de la plate-forme',1,1);




--
-- Table structure for table `action`
--

CREATE TABLE IF NOT EXISTS `action` (
  `id_action` int(11) NOT NULL AUTO_INCREMENT,
  `lib_action` varchar(255) NOT NULL,
  PRIMARY KEY (`id_action`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

--
-- Dumping data for table `action`
--

INSERT INTO `action` (`id_action`,`lib_action`) VALUES
(1,'Emprunter'),(2,'Retour');




--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `tach` (
  `id_tach` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_livre` int(11) NOT NULL,
  `dat_deb` date NOT NULL,
  `dat_ret` date NOT NULL,
  `id_action` int(11) NOT NULL,
  `id_biblio` int(11) NOT NULL,

  PRIMARY KEY (`id_tach`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;






