<?php 	



require_once 'core.php';
$idBiblio   = $_SESSION['biblioId'];

$sql = "SELECT tach.id_tach, user.nom_user, user.pre_user, livre.tit_livre, tach.dat_deb, tach.dat_ret, action.lib_action, user.det_user FROM tach 
		INNER JOIN user ON tach.id_user = user.id_user 
		INNER JOIN livre ON tach.id_livre = livre.id_livre
		INNER JOIN action ON tach.id_action = action.id_action 
		WHERE tach.id_biblio = '$idBiblio' AND tach.id_action = 1";

$result = $connect->query($sql);

$output = array('data' => array());

if($result->num_rows > 0) { 

 // $row = $result->fetch_array();
 $active = ""; 

 while($row = $result->fetch_array()) {
 	$id_empr = $row[0];
 	// active 
 	if($row[5] == date("Y-m-d")) {
 		// activate member
 		$active = "<label class='label label-warning'>Attention</label>";
 	} else if($row[5] > date("Y-m-d")){
 		// deactivate member
 		$active = "<label class='label label-success'>Valide</label>";
 	}else {
 		// deactivate member
 		$active = "<label class='label label-danger'>Fin du délai</label>";
 	} // /else

 	$button = '<!-- Single button -->
	<div class="btn-group">
	  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    Action <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu">
	    <li><a type="button" data-toggle="modal" id="editEmprModalBtn" data-target="#editEmprModal" onclick="editEmpr('.$id_empr.')"> <i class="glyphicon glyphicon-edit"></i> Modifier</a></li>
	    <li><a type="button" data-toggle="modal" data-target="#removeEmprModal" id="removeEmprModalBtn" onclick="removeEmpr('.$id_empr.')"> <i class="glyphicon glyphicon-trash"></i> Retour</a></li>       
	  </ul>
	</div>';

	// $brandId = $row[3];
	// $brandSql = "SELECT * FROM brands WHERE brand_id = $brandId";
	// $brandData = $connect->query($sql);
	// $brand = "";
	// while($row = $brandData->fetch_assoc()) {
	// 	$brand = $row['brand_name'];
	// }

	//$brand = $row[9];
	//$category = $row[10];

	//$imageUrl = substr($row[6], 3);
	//$livreImage = "<img class='img-round' src='".$imageUrl."' style='height:30px; width:50px;'  />";

 	$output['data'][] = array( 		
 	 		$row[1], 
 		// rate
 		$row[2],

 		$row[7],
 		// quantity 
 		$row[3],
 		$row[4], 		 	
 		$row[5],
 		$active,
 		//$row[6],
 		$button 		
 		); 	
 } // /while 

}// if num_rows

$connect->close();

echo json_encode($output);