<?php 	

require_once 'core.php';


$valid['success'] = array('success' => false, 'messages' => array());

$id_livre = $_POST['idLivre'];

if($id_livre) { 

 //$sql = "UPDATE product SET active = 2, status = 2 WHERE product_id = {$productId}";
 $sql = "DELETE FROM livre WHERE id_livre = {$id_livre}";

 if($connect->query($sql) === TRUE) {
 	$valid['success'] = true;
	$valid['messages'] = "Suppression effectué";		
 } else {
 	$valid['success'] = false;
 	$valid['messages'] = "Erreur lors de la suppression";
 }
 
 $connect->close();

 echo json_encode($valid);
 
} // /if $_POST
