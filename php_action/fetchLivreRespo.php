<?php 	



require_once 'core.php';
$idBiblio   = $_SESSION['biblioId'];

$sql = "SELECT id_livre, tit_livre, edi_livre, gen_livre, lan_livre, qte_livre, img_livre, act_livre, sta_livre, aut_livre FROM livre 
		WHERE sta_livre = 1 AND id_biblio = '$idBiblio'";

$result = $connect->query($sql);

$output = array('data' => array());

if($result->num_rows > 0) { 

 // $row = $result->fetch_array();
 $active = ""; 

 while($row = $result->fetch_array()) {
 	$idLivre = $row[0];
 	// active 
 	if($row[7] == 1) {
 		// activate member
 		$active = "<label class='label label-success'>Disponible</label>";
 	} else {
 		// deactivate member
 		$active = "<label class='label label-danger'>Indisponible</label>";
 	} // /else 

 	$button = '<!-- Single button -->
	<div class="btn-group">
	  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    Action <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu">
	    <li><a type="button" data-toggle="modal" id="editLivreModelBtn" data-target="#editLivreRespoModal" onclick="editLivre('.$idLivre.')"> <i class="glyphicon glyphicon-edit"></i> Modifier</a></li>
	    <li><a type="button" data-toggle="modal" data-target="#removeLivreRespoModel" id="removeLivreModelBtn" onclick="removeLivre('.$idLivre.')"> <i class="glyphicon glyphicon-trash"></i> Supprimer</a></li>       
	  </ul>
	</div>';

	// $brandId = $row[3];
	// $brandSql = "SELECT * FROM brands WHERE brand_id = $brandId";
	// $brandData = $connect->query($sql);
	// $brand = "";
	// while($row = $brandData->fetch_assoc()) {
	// 	$brand = $row['brand_name'];
	// }

	//$brand = $row[9];
	//$category = $row[10];

	$imageUrl = substr($row[6], 3);
	$livreImage = "<img class='img-round' src='".$imageUrl."' style='height:30px; width:50px;'  />";

 	$output['data'][] = array( 		
 		// image
 		$livreImage,
 		// product name
 		$row[1],
 		$row[9], 
 		// rate
 		$row[2],
 		// quantity 
 		$row[3],
 		$row[4], 		 	
 		$row[5],
 		//$row[6],	
 		// active
 		$active,
 		// button
 		$button 		
 		); 	
 } // /while 

}// if num_rows

$connect->close();

echo json_encode($output);