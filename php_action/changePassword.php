<?php 

require_once 'core.php';

if($_POST) {

	$valid['success'] = array('success' => false, 'messages' => array());

	$currentPassword = $_POST['password'];
	$newPassword = $_POST['npassword'];
	$conformPassword = $_POST['cpassword'];
	$userId = $_POST['id_user'];

	$sql ="SELECT * FROM user WHERE id_user = {$userId}";
	$query = $connect->query($sql);
	$result = $query->fetch_assoc();

	if($currentPassword == $result['pwd_user']) {

		if($newPassword == $conformPassword) {

			$updateSql = "UPDATE user SET pwd_user = '$newPassword' WHERE id_user = {$userId}";
			if($connect->query($updateSql) === TRUE) {
				$valid['success'] = true;
				$valid['messages'] = "Mise à jour du mot de passe";		
			} else {
				$valid['success'] = false;
				$valid['messages'] = "Echec";	
			}

		} else {
			$valid['success'] = false;
			$valid['messages'] = "le nouveau mot de passe et le mot de passe de confirmation ne sont pas identique";
		}

	} else {
		$valid['success'] = false;
		$valid['messages'] = "l'ancien mot de passe est incorrecte";
	}

	$connect->close();

	echo json_encode($valid);

}

?>