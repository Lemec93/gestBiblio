<?php 	

require_once 'core.php';


$valid['success'] = array('success' => false, 'messages' => array());

$idBiblio = $_POST['idBiblio'];

if($idBiblio) { 

 //$sql = "UPDATE biblio SET sta_biblio = 1 WHERE id_biblio = {$idBiblio}";
   $sql = "DELETE FROM biblio WHERE id_biblio = {$idBiblio}";

 if($connect->query($sql) === TRUE) {
 	$valid['success'] = true;
	$valid['messages'] = "Suppression effectué";		
 } else {
 	$valid['success'] = false;
 	$valid['messages'] = "Erreur lors de la suppression";
 }
 
 $connect->close();

 echo json_encode($valid);
 
} // /if $_POST