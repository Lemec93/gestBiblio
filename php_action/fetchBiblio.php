<?php 	

require_once 'core.php';

$sql = "SELECT id_biblio, nom_biblio, vil_biblio, act_biblio, sta_biblio FROM biblio WHERE sta_biblio = 1";
$result = $connect->query($sql);

$output = array('data' => array());

if($result->num_rows > 0) { 

 // $row = $result->fetch_array();
 $actBiblio = ""; 

 while($row = $result->fetch_array()) {
 	$idBiblio = $row[0];
 	// active 
 	if($row[3] == 1) {
 		// activate member
 		$actBiblio = "<label class='label label-success'>Disponible</label>";
 	} else {
 		// deactivate member
 		$actBiblio = "<label class='label label-danger'>Indisponible</label>";
 	}

 	$button = '<!-- Single button -->
	<div class="btn-group">
	  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    Action <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu">
	    <li><a type="button" data-toggle="modal" data-target="#editBiblioModel" onclick="editBiblio('.$idBiblio.')"> <i class="glyphicon glyphicon-edit"></i> Modifier</a></li>
	    <li><a type="button" data-toggle="modal" data-target="#removeMemberModal" onclick="removeBiblio('.$idBiblio.')"> <i class="glyphicon glyphicon-trash"></i> Supprimer</a></li>       
	  </ul>
	</div>';

 	$output['data'][] = array( 		
 		$row[1],
 		$row[2], 		
 		$actBiblio,
 		$button
 		); 	
 } // /while 

} // if num_rows

$connect->close();

echo json_encode($output);