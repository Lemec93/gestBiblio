<?php 	

require_once 'core.php';

$valid['success'] = array('success' => false, 'messages' => array());

if($_POST) {		

$id_biblio     = $_SESSION['biblioId']; 
 
$type = explode('.', $_FILES['editLivreRespoImage']['name']);
	$type = $type[count($type)-1];		
	$url = '../assests/images/stock/'.uniqid(rand()).'.'.$type;
	if(in_array($type, array('gif', 'jpg', 'jpeg', 'png', 'JPG', 'GIF', 'JPEG', 'PNG'))) {
		if(is_uploaded_file($_FILES['editLivreRespoImage']['tmp_name'])) {			
			if(move_uploaded_file($_FILES['editLivreRespoImage']['tmp_name'], $url)) {

				$sql = "UPDATE livre SET img_livre = '$url' WHERE id_biblio = $id_biblio";				

				if($connect->query($sql) === TRUE) {									
					$valid['success'] = true;
					$valid['messages'] = "Photo de couverture mise à jour";	
				} else {
					$valid['success'] = false;
					$valid['messages'] = "Echec lors de la mise à jour";
				}
			}	else {
				return false;
			}	// /else	
		} // if
	} // if in_array 		
	 
	$connect->close();

	echo json_encode($valid);
 
} // /if $_POST