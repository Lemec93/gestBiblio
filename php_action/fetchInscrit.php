<?php 	



require_once 'core.php';

$sql = "SELECT user.id_user, user.nom_user, user.pre_user, user.tel_user,
 		user.eml_user, user.pwd_user, user.pho_user, fonction.lib_fonc, biblio.nom_biblio FROM user 
		INNER JOIN fonction ON user.id_fonc = fonction.id_fonc 
		INNER JOIN biblio ON user.id_biblio = biblio.id_biblio 
		WHERE user.id_fonc = 3 OR user.id_fonc = 4";

//$sql = "SELECT id_user, nom_user, pre, act_biblio, sta_biblio FROM biblio WHERE sta_biblio = 1";

$result = $connect->query($sql);

$output = array('data' => array());

if($result->num_rows > 0) { 

 // $row = $result->fetch_array();
 //$active = ""; 

 while($row = $result->fetch_array()) {
 	$id_respo = $row[0];
 	// active 
 	/*if($row[7] == 1) {
 		// activate member
 		$active = "<label class='label label-success'>Available</label>";
 	} else {
 		// deactivate member
 		$active = "<label class='label label-danger'>Not Available</label>";
 	} // /else*/
/*
 	$button = '<!-- Single button -->
	<div class="btn-group">
	  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    Action <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu">
	    <li><a type="button" data-toggle="modal" id="editRespoModalBtn" data-target="#editRespoModal" onclick="editRespo('.$id_respo.')"> <i class="glyphicon glyphicon-edit"></i> Modifier</a></li>
	    <li><a type="button" data-toggle="modal" data-target="#removeRespoModal" id="removeRespoModalBtn" onclick="removeRespo('.$id_respo.')"> <i class="glyphicon glyphicon-trash"></i> Supprimer</a></li>       
	  </ul>
	</div>';*/

	// $brandId = $row[3];
	// $brandSql = "SELECT * FROM brands WHERE brand_id = $brandId";
	// $brandData = $connect->query($sql);
	// $brand = "";
	// while($row = $brandData->fetch_assoc()) {
	// 	$brand = $row['brand_name'];
	// }

	$libFonc = $row[8];
	$nomBiblio = $row[7];

	$imageUrl = substr($row[6], 3);
	$respoImage = "<img class='img-round' src='".$imageUrl."' style='height:30px; width:50px;'  />";

 	$output['data'][] = array( 		
 		// image
 		$respoImage,
 		// Nom Respo
 		$row[1], 
 		//  Prénoms Respo
 		$row[2], 
 		//Contact Respo
 		$row[3],
 		//Email Respo
 		$row[4],
 		// Mot de passe 
 		$row[5], 		 	
 		// Nom bibliothèque
 		$nomBiblio,
 		// category 		
 	//	$category,
 		// active
 		//$active,
 		// button
 		//$button 
 		$libFonc		
 		); 	
 } // /while 

}// if num_rows

$connect->close();

echo json_encode($output);