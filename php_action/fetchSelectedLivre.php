<?php 	

require_once 'core.php';

$id_livre = $_POST['idLivre'];

$sql = "SELECT id_livre, tit_livre, aut_livre, edi_livre, gen_livre, lan_livre, qte_livre, res_livre, img_livre, act_livre, sta_livre, id_biblio FROM livre WHERE id_livre = {$id_livre}";
$result = $connect->query($sql);

if($result->num_rows > 0) { 
 $row = $result->fetch_array();
} // if num_rows

$connect->close();

echo json_encode($row);