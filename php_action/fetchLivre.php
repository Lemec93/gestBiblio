<?php 	



require_once 'core.php';

$sql = "SELECT livre.id_livre, livre.tit_livre, livre.edi_livre, livre.gen_livre, livre.lan_livre, livre.qte_livre, livre.img_livre, livre.res_livre, biblio.nom_biblio FROM livre
	INNER JOIN biblio ON livre.id_biblio = biblio.id_biblio  
		WHERE sta_livre = 1";

$result = $connect->query($sql);

$output = array('data' => array());

if($result->num_rows > 0) { 

 // $row = $result->fetch_array();
 //$active = $row[7]; 

 while($row = $result->fetch_array()) {
 	$id_livre = $row[0];
 	
 	// active 
 	/*if($row[7] == 1) {
 		// activate member
 		$active = "<label class='label label-success'>Disponible</label>";
 	} else {
 		// deactivate member
 		$active = "<label class='label label-danger'>Indisponible</label>";
 	} // /else

 	$button = '<!-- Single button -->
	<div class="btn-group">
	  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    Action <span class="caret"></span>
	  </button>
	  <ul class="dropdown-menu">
	    <li><a type="button" data-toggle="modal" id="editLivreModalBtn" data-target="#editLivreModal" onclick="editLivre('.$id_livre.')"> <i class="glyphicon glyphicon-edit"></i> Modifier</a></li>
	    <li><a type="button" data-toggle="modal" data-target="#removeLivreModal" id="removeLivreModalBtn" onclick="removeLivre('.$id_livre.')"> <i class="glyphicon glyphicon-trash"></i> Supprimer</a></li>       
	  </ul>
	</div>';*/

	// $brandId = $row[3];
	// $brandSql = "SELECT * FROM brands WHERE brand_id = $brandId";
	// $brandData = $connect->query($sql);
	// $brand = "";
	// while($row = $brandData->fetch_assoc()) {
	// 	$brand = $row['brand_name'];
	// }

	//$brand = $row[9];
	//$category = $row[10];

	$imageUrl = substr($row[6], 3);
	$livreImage = "<img class='img-round' src='".$imageUrl."' style='height:30px; width:50px;'  />";

 	$output['data'][] = array( 		
 		// image
 		$livreImage,
 		// product name
 		$row[1], 
 		// rate
 		$row[2],
 		// quantity 
 		$row[3],
 		$row[4], 		 	
 		$row[5],
 		//$row[6],	
 		// active
 		$row[7],
 		// button
 		$row[8] 		
 		); 	
 } // /while 

}// if num_rows

$connect->close();

echo json_encode($output);