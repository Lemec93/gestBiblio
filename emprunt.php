<?php require_once 'includes/headerRespo.php'; ?>


<div class="row">
	<div class="col-md-12">

		<ol class="breadcrumb">
		  <li><a href="dashboardRespo.php">Accueil</a></li>		  
		  <li class="active">Emprunt</li>
		</ol>

		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="page-heading"> <i class="glyphicon glyphicon-edit"></i> Gestion des emprunts</div>
			</div> <!-- /panel-heading -->
			<div class="panel-body">

				<div class="remove-messages"></div>

				<div class="div-action pull pull-right" style="padding-bottom:20px;">
					<button class="btn btn-default button1" data-toggle="modal" data-target="#addEmprModel"> <i class="glyphicon glyphicon-plus-sign"></i> Ajouter emprunt </button>
				</div> <!-- /div-action -->				
				
				<table class="table" id="manageEmprTable">
					<thead>
						<tr>							
							<th>Nom</th>
							<th>Prénoms</th>
							<th>Detail</th>
							<th>Livre</th>
							<th>Date sortie</th>
							<th>Date retour</th>
							<th>Etat</th>
							<th style="width:15%;">Options</th>
						</tr>
					</thead>
				</table>
				<!-- /table -->

			</div> <!-- /panel-body -->
		</div> <!-- /panel -->		
	</div> <!-- /col-md-12 -->
</div> <!-- /row -->

<div class="modal fade" id="addEmprModel" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    	
    	<form class="form-horizontal" id="submitEmprForm" action="php_action/createEmpr.php" method="POST">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-plus"></i> Ajouter emprunt</h4>
	      </div>
	      <div class="modal-body">

	      	<div id="add-empr-messages"></div>

	        <div class="form-group">
	        	<label for="slt_insc" class="col-sm-3 control-label">Inscrits: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <select type="text" class="form-control" id="slt_insc" placeholder="Inscrits" name="slt_insc" >
				      	<option value="">~~SELECT~~</option>
				      	<?php 
				      	$idBiblio   = $_SESSION['biblioId'];
				      	$sql = "SELECT id_user, nom_user, pre_user FROM user WHERE id_biblio = '$idBiblio'";
								$result = $connect->query($sql);

								while($row = $result->fetch_array()) {
									echo "<option value='".$row[0]."'>".$row[1]." ".$row[2]."</option>";
								} // while
								
				      	?>
				      </select>
				    </div>
	        </div> <!-- /form-group-->

	        <div class="form-group">
	        	<label for="slt_livre" class="col-sm-3 control-label">Livres: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <select type="text" class="form-control" id="slt_livre" placeholder="Livre" name="slt_livre" >
				      	<option value="">~~SELECT~~</option>
				      	<?php 
				      	$idBiblio   = $_SESSION['biblioId'];
				      	$sql = "SELECT id_livre, tit_livre FROM livre WHERE id_biblio = '$idBiblio' AND sta_livre = 1 AND act_livre = 1";
								$result = $connect->query($sql);

								while($row = $result->fetch_array()) {
									echo "<option value='".$row[0]."'>".$row[1]."</option>";
								} // while
								
				      	?>
				      </select>
				    </div>
	        </div> <!-- /form-group-->


	        <div class="form-group">
	        	<label for="dat_ret" class="col-sm-3 control-label">Date retour: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="date" class="form-control" id="dat_ret" min=<?php echo date('Y-m-d');?> placeholder="Date retour" name="dat_ret" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->        	        

	      </div> <!-- /modal-body -->
	      
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
	        
	        <button type="submit" class="btn btn-primary" id="addEmprModalBtn" data-loading-text="Loading..." autocomplete="off">Enrégistrer</button>
	      </div>
	      <!-- /modal-footer -->
     	</form>
	     <!-- /.form -->
    </div>
    <!-- /modal-content -->
  </div>
  <!-- /modal-dailog -->
</div>
<!-- / add modal --> 

<!-- edit brand -->
<div class="modal fade" id="editEmprModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    	
    	<form class="form-horizontal" id="editEmprForm" action="php_action/editEmpr.php" method="POST">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><i class="fa fa-edit"></i> Modifier Emprunt</h4>
	      </div>
	      <div class="modal-body">

	      	<div id="edit-empr-messages"></div>

	      	<div class="modal-loading div-hide" style="width:50px; margin:auto;padding-top:50px; padding-bottom:50px;">
						<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
						<span class="sr-only">Chargement...</span>
					</div>

			<div class="edit-empr-result">
			<div class="form-group">
	        	<label for="edit_slt_insc" class="col-sm-3 control-label">Inscrits: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <select type="text" class="form-control" id="edit_slt_insc" placeholder="Inscrits" name="edit_slt_insc" >
				      	<option value="">~~SELECT~~</option>
				      	<?php 
				      	$idBiblio   = $_SESSION['biblioId'];
				      	$sql = "SELECT id_user, nom_user, pre_user FROM user WHERE id_biblio = '$idBiblio'";
								$result = $connect->query($sql);

								while($row = $result->fetch_array()) {
									echo "<option value='".$row[0]."'>".$row[1]." ".$row[2]."</option>";
								} // while
								
				      	?>
				      </select>
				    </div>
	        </div> <!-- /form-group-->

	        <div class="form-group">
	        	<label for="edit_slt_livre" class="col-sm-3 control-label">Livres: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <select type="text" class="form-control" id="edit_slt_livre" placeholder="Livre" name="edit_slt_livre" >
				      	<option value="">~~SELECT~~</option>
				      	<?php 
				      	$idBiblio   = $_SESSION['biblioId'];
				      	$sql = "SELECT id_livre, tit_livre FROM livre WHERE id_biblio = '$idBiblio' AND sta_livre = 1 AND act_livre = 1";
								$result = $connect->query($sql);

								while($row = $result->fetch_array()) {
									echo "<option value='".$row[0]."'>".$row[1]."</option>";
								} // while
								
				      	?>
				      </select>
				    </div>
	        </div> <!-- /form-group-->


		        <div class="form-group">
	        	<label for="edit_dat_ret" class="col-sm-3 control-label">Date retour: </label>
	        	<label class="col-sm-1 control-label">: </label>
				    <div class="col-sm-8">
				      <input type="date" class="form-control" id="edit_dat_ret" placeholder="Date retour" name="edit_dat_ret" autocomplete="off">
				    </div>
	        </div> <!-- /form-group-->	
		      </div>         	        
		      <!-- /edit brand result -->

	      </div> <!-- /modal-body -->
	      
	      <div class="modal-footer editEmprFooter">
	        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Annuler</button>
	        
	        <button type="submit" class="btn btn-success" id="editEmprBtn" data-loading-text="Loading..." autocomplete="off"> <i class="glyphicon glyphicon-ok-sign"></i> Enrégistrer</button>
	      </div>
	      <!-- /modal-footer -->
     	</form>
	     <!-- /.form -->
    </div>
    <!-- /modal-content -->
  </div>
  <!-- /modal-dailog -->
</div>
<!-- / add modal -->
<!-- /edit brand -->

<!-- remove brand -->
<div class="modal fade" tabindex="-1" role="dialog" id="removeEmprModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="glyphicon glyphicon-trash"></i> Dépôt</h4>
      </div>
      <div class="modal-body">

      	<div class="removeRespoMessages"></div>
      	
        <p>Retour du livre !</p>
      </div>
      <div class="modal-footer removeEmprFooter">
        <button type="button" class="btn btn-default" data-dismiss="modal"> <i class="glyphicon glyphicon-remove-sign"></i> Annuler</button>
        <button type="button" class="btn btn-primary" id="removeEmprBtn" data-loading-text="Loading..."> <i class="glyphicon glyphicon-ok-sign"></i> Enrégistrer</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- /remove brand -->

<script src="custom/js/emprunt.js"></script>

<?php require_once 'includes/footer.php'; ?>