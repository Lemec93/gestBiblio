<?php require_once 'includes/headerUser.php'; ?>


<div class="row">
	<div class="col-md-12">

		<ol class="breadcrumb">
		  <li><a href="dashboardUser.php">Accueil</a></li>		  
		  <li class="active">Livre</li>
		</ol>

		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="page-heading"> <i class="glyphicon glyphicon-edit"></i> Bibliothèque locale</div>
			</div> <!-- /panel-heading -->
			<div class="panel-body">

				<div class="remove-messages"></div>
				
				
				<table class="table row" id="manageLivreUserTable" style="width:100%;">
					<thead>
						<tr>							
							<th>Image</th>
							<th>Titre</th>
							<!--th>Auteur</th-->
							<th>Genre</th>
							<!--th>Langue</th-->
							<!--th>Editeur</th-->
							<!--th>Résume</th-->
							<th style="width:15%;">Options</th>
						</tr>
					</thead>
					
				</table>
				<!-- /table -->

			</div> <!-- /panel-body -->
		</div> <!-- /panel -->		
	</div> <!-- /col-md-12 -->
</div> <!-- /row -->


<div class="modal fade" id="affLivre" tabindex="-1" role="dialog">
<aside class="profile-card">
	<header>
    <!-- here’s the avatar -->
    	<a target="_blank" href="#">
      		<img id="imgliv" src="assests/images/Autres/imgBiblioLocal.jpg" class="hoverZoomLink">
    	</a>
	    <!-- the username -->
	    <h1 id="tit"></h1>
	    <!-- and role or location -->
	    <h2 id="aut"></h2>
	    <span>
	    	<span id="gen"></span>,
	    	<span id="lan"></span>,
	    	<span id="edi"></span>
	    </span>
	    	
	</header>
  <!-- bit of a bio; who are you? -->
	<div class="profile-bio">
		<p id="res"></p>
	</div>
  <!-- some social links to show off -->
	<ul class="profile-social-links">
    	<li>
    		<a target="_blank" href="https://www.facebook.com/profile.php?id=100008152065270">
    			<i class="fa fa-facebook"></i>
     		</a>
    	</li>
    	<li>
      		<a target="_blank" href="https://twitter.com/HTERCUMANP">
        		<i class="fa fa-twitter"></i>
      		</a>
    	</li>
    	<li>
      		<a target="_blank" href="https://github.com/harunpehlivan">
        		<i class="fa fa-github"></i>
      		</a>
    	</li>
    	<li>
      		<a target="_blank" href="https://www.behance.net/harunpehlivan">
       			<i class="fa fa-behance"></i>
      		</a>
    	</li>
  	</ul>
</aside>
</div>


<script src="custom/js/livreUser.js"></script>

<?php require_once 'includes/footer.php'; ?>