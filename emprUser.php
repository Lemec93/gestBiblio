<?php require_once 'includes/headerUser.php'; ?>


<div class="row">
	<div class="col-md-12">

		<ol class="breadcrumb">
		  <li><a href="dashboardUser.php">Accueil</a></li>		  
		  <li class="active">Emprunt</li>
		</ol>

		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="page-heading"> <i class="glyphicon glyphicon-edit"></i> Gestion des emprunt</div>
			</div> <!-- /panel-heading -->
			<div class="panel-body">

				<div class="remove-messages"></div>			
				
				<table class="table" id="manageEmprUserTable" style="width:100%;">
					<thead>
						<tr>
							<th>Livre</th>
							<th>Date emprunt</th>
							<th>Date retour</th>
							<th>Etat</th>
						</tr>
					</thead>
				</table>
				<!-- /table -->

			</div> <!-- /panel-body -->
		</div> <!-- /panel -->		
	</div> <!-- /col-md-12 -->
</div> <!-- /row -->

<script src="custom/js/emprUser.js"></script>

<?php require_once 'includes/footer.php'; ?>