<?php require_once 'includes/headerUser.php'; ?>

<?php 

$id_user = $_SESSION['userId'];

      $sqlCard = "SELECT user.nom_user, user.pre_user, user.tel_user, user.eml_user, user.pho_user, fonction.lib_fonc FROM user INNER JOIN fonction ON user.id_fonc = fonction.id_fonc WHERE user.id_user = $id_user";
          $resultCard = $connect->query($sqlCard);

          while($row = $resultCard->fetch_array()) {
            $nom = $row[0];
            $prenom = $row[1];
            $contact = $row[2];
            $mail = $row[3];
            $urlPhoto = $row[4];
            $fonction = $row[5];

              } // while

$connect->close();

?>

<style type="text/css">

body, html{
     height: 100%;
 	font-family: 'Oxygen', sans-serif;
}

.main{
 	margin-top: 70px;
}

h1.title { 
	font-size: 50px;
	font-family: 'Passion One', cursive; 
	font-weight: 400; 
}

hr{
	width: 10%;
	color: #fff;
}

.form-group{
	margin-bottom: 15px;
}

label{
	margin-bottom: 15px;
}

input,
input::-webkit-input-placeholder {
    font-size: 11px;
    padding-top: 3px;
}

.main-login{
 	background-color: #fff;
    
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);

}

.main-center{
 	margin-top: 30px;
 	margin: 0 auto;
 	max-width: 330px;
    padding: 40px 40px;

}

.login-button{
	margin-top: 5px;
}

.login-register{
	font-size: 11px;
	text-align: center;
}

</style>

<!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="assests/plugins/fullcalendar/fullcalendar.min.css">
    <link rel="stylesheet" href="custom/css/carte.css"/>
    <link rel="stylesheet" href="assests/plugins/fullcalendar/fullcalendar.print.css" media="print">


<div class="row">
	
	<div class="corps">
		<center>
			<h1>BIENVENU A LA PAGE D'ACCUEIL</h1>
		</center><br/><br/>
		
			 <div class="container" id="tourpackages-carousel">
      
      <div class="row">
        
        <div class="col-xs-18 col-sm-6 col-md-3">
          <div class="container card" style='height:360px;'>
            <div class="row">
              <div class="">

                <div class="card hovercard">
                  <div class="cardheader"></div>
                  <div class="avatar">
                    <img alt="" src="<?php echo "Etape/",$urlPhoto;?>" style='height:120px; width:100px;'>
                  </div>
                  <div class="info">
                    <div class="title">
                        <a target="_blank" href=""><?php echo $nom," ",$prenom;?></a>
                    </div>
                    <div class="desc"><b><?php echo $fonction ?></b></div>
                    <div class="desc">Contact: <?php echo $contact ?></div>
                    <div class="desc">Email: <?php echo $mail ?></div>
                  </div>
                  <div class="bottom">
                    <a class="btn btn-primary btn-twitter btn-sm" href="https://twitter.com/" title="Compte twitter du groupe-ITA">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a class="btn btn-danger btn-sm" rel="publisher"
                       href="https://plus.google.com/" title="Compte google plus du groupe-ITA">
                        <i class="fa fa-google-plus"></i>
                    </a>
                    <a class="btn btn-primary btn-sm" rel="publisher"
                       href="https://facebook.com/" title="Compte facebook du groupe-ITA">
                        <i class="fa fa-facebook"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          <marquee scrollamount="4"><strong>Flash Info:</strong> La cérémonie de lancement des soutenances 2016-2017 aura lieu le 28 Octobre 2017 à partir de 8H45 au sein du GROUPE ITA annexe Bouaké.</marquee>
          </div>
        </div>

        <div class="col-xs-18 col-sm-6 col-md-3">
          <div class="thumbnail">
            <img src="assests/images/Autres/imgBiblioLocal.jpg" alt="">
              <div class="caption">
                <h4>Bibliothèque locale</h4><br/>
				<b>Consulter les livres</b>
                <p>Veuillez cliquer sur le bouton "Consulter" pour consulter et réserver l'enssemble des livres et ouvrages disponible dans votre bibliothèque.</p>
                <p><a href="livreUser.php" class="btn-info btn-xs" role="button">Consulter</a></p>
            </div>
          </div>
        </div>

        <div class="col-xs-18 col-sm-6 col-md-3">
          <div class="thumbnail">
            <img src="assests/images/Autres/imgListe.jpg" alt="">
              <div class="caption">
                <h4>Liste</h4><br/>
				<b>Livre Emprunter</b>
                <p>Veuillez cliquer sur le bouton "Accéder" pour avoir la liste de l'enssemble des ouvrages que vous avez emprunter.</p>
                <p><a href="emprUser.php" class="btn-info btn-xs" role="button">Accéder</a></p>
            </div>
          </div>
        </div>

        <div class="col-xs-18 col-sm-6 col-md-3">
          <div class="thumbnail">
            <img src="assests/images/Autres/imgITA.png" alt="">
              <div class="caption">
                <h4>Groupe ITA</h4><br/>
				<b>Site web officiel</b>
                <p>Plus besoin de se déplacer pour suivre les cours, le GROUPE ITA INGENIERIE vous propose les cours à distance, partout via internet.</p>
                <p><a href="http://www.ita-education.ci" target="_blank" class="btn-info btn-xs" role="button">Accéder</a></p>
            </div>
          </div>
        </div>
        
      </div><!-- End row -->
      
    </div><!-- End container -->
		</div>
	
</div> <!--/row-->


<script type="text/javascript">
	$(function () {
			// top bar active
	$('#navDashboardUser').addClass('active');

    });
</script>

<?php require_once 'includes/footer.php'; ?>