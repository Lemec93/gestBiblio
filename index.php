<?php 
require_once 'php_action/db_connect.php';

session_start();


if(isset($_SESSION['userId'])) {
	if($_SESSION['fonc'] === 1) {
		header('location: http://localhost:80/Etape/dashboard.php');
		}elseif($_SESSION['fonc'] === 2){
			header('location: http://localhost:80/Etape/dashboardRespo.php');
		}elseif ($_SESSION['fonc'] === 3) {
			header('location: http://localhost:80/Etape/dashboardUser.php');
		}
		
}


$errors = array();

if($_POST) {		

	$eml = $_POST['eml_user'];
	$pwd = $_POST['pwd_user'];

	if(empty($eml) && empty($pwd)) {
		if($eml == "") {
			$errors[] = "Entrez votre Email";
		} 

		if($pwd == "") {
			$errors[] = "Entrez votre passe";
		}
	} else {
		$sql = "SELECT * FROM user WHERE eml_user = '$eml'";
		$result = $connect->query($sql);

		if($result->num_rows == 1) {
			//$password = md5($password);
			// exists
			$mainSql = "SELECT * FROM user WHERE eml_user = '$eml' AND pwd_user = '$pwd'";
			$mainResult = $connect->query($mainSql);

			if($mainResult->num_rows == 1) {
				$value = $mainResult->fetch_assoc();
				$id_user = $value['id_user'];
				$id_biblio = $value['id_biblio'];
				// set session
				$_SESSION['userId'] = $id_user;
				$_SESSION['biblioId'] = $id_biblio;
				//$errors[] = "Succes user ="+$user_id+" pass=";
				

				$adminSql = "SELECT * FROM user WHERE id_user = '$id_user' AND id_fonc = 1";
				$adminResult = $connect->query($adminSql);

				$respoSql = "SELECT * FROM user WHERE id_user = '$id_user' AND id_fonc = 2";
				$respoResult = $connect->query($respoSql);

				if($adminResult->num_rows == 1){
					header('location: http://localhost:80/Etape/dashboard.php');
					$_SESSION['fonc'] = 1;
				}elseif($respoResult->num_rows == 1){
					header('location: http://localhost:80/Etape/dashboardRespo.php');
					$_SESSION['fonc'] = 2;
				}else{
					header('location: http://localhost:80/Etape/dashboardUser.php');
					$_SESSION['fonc'] = 3;
				}

			} else{	
				$errors[] = "Combinaison email/passe incorrecte";
			} // /else
		} else {		
			$errors[] = "email inéxistant";		
		} // /else
	} // /else not empty username // password
	
} // /if $_POST
?>

<!DOCTYPE html>
<html>
<head>
	<title>Gestion Bibliothèque</title>


	<meta charset="utf-8"/>
	<script type="text/javascript"></script>

	<!-- bootstrap -->
	<link rel="stylesheet" href="assests/bootstrap/css/bootstrap.min.css">
	<!-- bootstrap theme-->
	<link rel="stylesheet" href="assests/bootstrap/css/bootstrap-theme.min.css">
	<!-- font awesome -->
	<link rel="stylesheet" href="assests/font-awesome/css/font-awesome.min.css">

  <!-- custom css -->
  <link rel="stylesheet" href="custom/css/custom.css">	

  <!-- jquery -->
	<script src="assests/jquery/jquery.min.js"></script>
  <!-- jquery ui -->  
  <link rel="stylesheet" href="assests/jquery-ui/jquery-ui.min.css">
  <script src="assests/jquery-ui/jquery-ui.min.js"></script>

  <!-- bootstrap js -->
	<script src="assests/bootstrap/js/bootstrap.min.js"></script>
	<script src="custom/js/user.js"></script>
	<style type="text/css">
		body{
			background-image: url('assests/images/bg.png');	
		}

		.bgd{
			width: 100%;
			background-color: rgba(0,0,0,0.6) !important;
		}

	</style>
</head>
<body>
	<div class="bgd" >
	<div class="container">
	<center><h1 style="color: #fff">Bibliothèque du groupe ITA-INGENIERIE SA</h1></center>
		<div class="row vertical">
		
			<div class="col-md-5 col-md-offset-4">
				<div class="panel panel-info">
					<div class="panel-heading">
					
						<h3 class="panel-title">Connectez-vous ici</h3>
					</div>
					<div class="panel-body">

						<div class="messages">
							<?php if($errors) {
								foreach ($errors as $key => $value) {
									echo '<div class="alert alert-warning" role="alert">
									<i class="glyphicon glyphicon-exclamation-sign"></i>
									'.$value.'</div>';										
									}
								} ?>
						</div>

						<form class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" id="loginForm">
							<fieldset>
							  <div class="form-group">
									<label for="eml_user" class="col-sm-2 control-label">Email</label>
									<div class="col-sm-10">
									  <input type="text" class="form-control" id="eml_user" name="eml_user" placeholder="Ex: xxx@email.com" autocomplete="off" />
									</div>
								</div>
								<div class="form-group">
									<label for="pwd_user" class="col-sm-2 control-label">Passe</label>
									<div class="col-sm-10">
									  <input type="password" class="form-control" id="pwd_user" name="pwd_user" placeholder="Password" autocomplete="off" />
									</div>
								</div>								
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
									  <button type="submit" class="btn btn-default"> <i class="glyphicon glyphicon-log-in"></i> Connexion</button>
									</div>
								</div>
							</fieldset>
						</form>
						
					</div><!-- panel-body -->
					<marquee><h3>
							Pour tous problèmes d'authentification, veuillez contactez votre responsable de bibliothèque. Le groupe ITA vous remercie!!!
						</h3></marquee>
				</div>
				<!-- /panel -->
			</div>
			<!-- /col-md-4 -->
		</div>
		<!-- /row -->
	</div>
	<!-- container -->
	</div>	
</body>
</html>







	