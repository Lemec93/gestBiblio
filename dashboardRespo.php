<?php require_once 'includes/headerRespo.php'; ?>

<?php 

$idBiblio   = $_SESSION['biblioId'];


$sql = "SELECT * FROM biblio WHERE sta_biblio = 1 AND act_biblio = 1";
$query = $connect->query($sql);
$countProduct = $query->num_rows;

$orderSql = "SELECT * FROM user WHERE (id_fonc = 3 OR id_fonc = 4) AND id_biblio = '$idBiblio'";
$orderQuery = $connect->query($orderSql);
$countOrder = $orderQuery->num_rows;


$lowStockSql = "SELECT sum(qte_livre) FROM livre WHERE id_biblio = '$idBiblio'";
$lowStockQuery = $connect->query($lowStockSql);
$countLowStoc = $lowStockQuery->fetch_assoc();
$countLowStock = $countLowStoc['sum(qte_livre)'];

$nbrEmprSql = "SELECT count(tach.id_tach) FROM tach INNER JOIN action ON tach.id_action = action.id_action WHERE tach.id_biblio = '$idBiblio' AND tach.id_action = 1";
$nbrEmprQuery = $connect->query($nbrEmprSql);
$countEmpr = $nbrEmprQuery->fetch_assoc();
$countEmp = $countEmpr['count(tach.id_tach)'];
$date = date("Y-m-d");

$delaiSql = "SELECT count(id_tach) FROM tach WHERE (id_biblio = $idBiblio AND id_action = 1 AND dat_ret < '$date')";
$delaiQuery = $connect->query($delaiSql);
$countdelai = $delaiQuery->fetch_assoc();
$countdelaipass = $countdelai['count(id_tach)'];


$id_user = $_SESSION['userId'];

			$sqlCard = "SELECT user.nom_user, user.pre_user, user.tel_user, user.eml_user, user.pho_user, fonction.lib_fonc FROM user INNER JOIN fonction ON user.id_fonc = fonction.id_fonc WHERE user.id_user = $id_user";
					$resultCard = $connect->query($sqlCard);

					while($row = $resultCard->fetch_array()) {
						$nom = $row[0];
						$prenom = $row[1];
						$contact = $row[2];
						$mail = $row[3];
						$urlPhoto = $row[4];
						$fonction = $row[5];

							} // while

			$sqlBiblio = "SELECT nom_biblio, vil_biblio FROM biblio WHERE id_biblio = '$idBiblio'";
				$resultBiblio = $connect->query($sqlBiblio);
				while($row = $resultBiblio->fetch_array()){
					$nomBiblio = $row[0];
					$vilBiblio = $row[1];
				}


$connect->close();

?>


<style type="text/css">
	.ui-datepicker-calendar {
		display: none;
	}
</style>

<!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="assests/plugins/fullcalendar/fullcalendar.min.css">
    <link rel="stylesheet" href="custom/css/carte.css"/>
    <link rel="stylesheet" href="assests/plugins/fullcalendar/fullcalendar.print.css" media="print">


<div class="row">
	
	<div class="col-md-3">
		<div class="panel panel-success">
			<div class="panel-heading">
				
				<a href="inscription.php" style="text-decoration:none;color:black;">
					Total des inscrits
					<span class="badge pull pull-right"><?php echo $countOrder; ?></span>	
				</a>
				
			</div> <!--/panel-hdeaing-->
		</div> <!--/panel-->
	</div> <!--/col-md-4-->

		<div class="col-md-3">
			<div class="panel panel-info">
			<div class="panel-heading">
				<a href="livreRespo.php" style="text-decoration:none;color:black;">
					Total des livres
					<span class="badge pull pull-right"><?php echo $countLowStock; ?></span>
				</a>
					
			</div> <!--/panel-hdeaing-->
		</div> <!--/panel-->
		</div> <!--/col-md-4-->

	<div class="col-md-3">
		<div class="panel panel-warning">
			<div class="panel-heading">
				<a href="emprunt.php" style="text-decoration:none;color:black;">
					Total emprunt
					<span class="badge pull pull-right"><?php echo $countEmp; ?></span>	
				</a>
				
			</div> <!--/panel-hdeaing-->
		</div> <!--/panel-->
	</div> <!--/col-md-4-->

	<div class="col-md-3">
		<div class="panel panel-danger">
			<div class="panel-heading">
				<a href="emprunt.php" style="text-decoration:none;color:black;">
					Fin du délai d'emprunt
					<span class="badge pull pull-right"><?php echo $countdelaipass; ?></span>	
				</a>
				
			</div> <!--/panel-hdeaing-->
		</div> <!--/panel-->
	</div> <!--/col-md-4-->

	
	<!--BLOC 1-->
	<div class="col-md-4">
		<div class="container card">
	<div class="row">
		<div class="">

            <div class="card hovercard">
                <div class="cardheader">

                </div>
                <div class="avatar">
                    <img alt="" src=<?php echo "Etape/",$urlPhoto;?>>
                </div>
                <div class="info">
                    <div class="title">
                        <a target="_blank" href=""><?php echo $nom," ",$prenom;?></a>
                    </div>
                    <div class="desc"><b><?php echo $fonction ?></b></div>
                    <div class="desc">Contact: <?php echo $contact ?></div>
                    <div class="desc">Email: <?php echo $mail ?></div>
                </div>
                <div class="bottom">
                    <a class="btn btn-primary btn-twitter btn-sm" href="https://twitter.com/" title="Compte twitter du groupe-ITA">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a class="btn btn-danger btn-sm" rel="publisher"
                       href="https://plus.google.com/" title="Compte google plus du groupe-ITA">
                        <i class="fa fa-google-plus"></i>
                    </a>
                    <a class="btn btn-primary btn-sm" rel="publisher"
                       href="https://facebook.com/" title="Compte facebook du groupe-ITA">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a class="btn btn-warning btn-sm" rel="publisher" href="https://plus.google.com/">
                        <i class="fa fa-behance"></i>
                    </a>
                </div>
            </div>

        </div>

	</div>
</div>
		<br/>

		<div class="card">
		  <div class="cardHeader" style="background-color:#245580;">
		    <h2><?php echo $nomBiblio;?></h2>
		    <h3>(<?php echo $vilBiblio;?>)</h3>
		  </div>

		  <div class="cardContainer">
		    <p> <i class="glyphicon glyphicon"></i>Bibliothèque</p>
		  </div>
		</div>

	</div>
	
	<!--Calendrier-->
<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading"> <i class="glyphicon glyphicon-calendar"></i> Calendar</div>
			<div class="panel-body">
				<div id="calendar"></div>
			</div>	
		</div>
		
	</div>
	
</div> <!--/row-->

<!-- fullCalendar 2.2.5 -->
<script src="assests/plugins/moment/moment.min.js"></script>
<script src="assests/plugins/fullcalendar/fullcalendar.min.js"></script>


<script type="text/javascript">
	$(function () {
			// top bar active
	$('#navDashboardRespo').addClass('active');

      //Date for the calendar events (dummy data) 
      var date = new Date();
      var d = date.getDate(),
      m = date.getMonth(),
      y = date.getFullYear();

      $('#calendar').fullCalendar({
        header: {
          left: '',
          center: 'title'
        },
        buttonText: {
          today: 'today',
          month: 'month'          
        }        
      });


    });
</script>

<?php require_once 'includes/footer.php'; ?>