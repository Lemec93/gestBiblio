<?php require_once 'php_action/db_connect.php' ?>
<?php require_once 'includes/header.php'; ?>

<?php 

$idBiblio   = $_SESSION['biblioId'];

$sql = "SELECT * FROM biblio WHERE sta_biblio = 1 AND act_biblio = 1";
$query = $connect->query($sql);
$countProduct = $query->num_rows;

$orderSql = "SELECT * FROM user WHERE id_fonc = 3 OR id_fonc = 4";
$orderQuery = $connect->query($orderSql);
$countOrder = $orderQuery->num_rows;


$lowStockSql = "SELECT sum(qte_livre) FROM livre";
$lowStockQuery = $connect->query($lowStockSql);
$countLowStoc = $lowStockQuery->fetch_assoc();
$countLowStock = $countLowStoc['sum(qte_livre)'];


$id_user = $_SESSION['userId'];

			$sqlCard = "SELECT user.nom_user, user.pre_user, user.tel_user, user.eml_user, user.pho_user, fonction.lib_fonc FROM user INNER JOIN fonction ON user.id_fonc = fonction.id_fonc WHERE user.id_user = $id_user";
					$resultCard = $connect->query($sqlCard);

					while($row = $resultCard->fetch_array()) {
						$nom = $row[0];
						$prenom = $row[1];
						$contact = $row[2];
						$mail = $row[3];
						$urlPhoto = $row[4];
						$fonction = $row[5];

							} // while

$sqlBiblio = "SELECT nom_biblio, vil_biblio FROM biblio WHERE id_biblio = '$idBiblio'";
				$resultBiblio = $connect->query($sqlBiblio);
				while($row = $resultBiblio->fetch_array()){
					$nomBiblio = $row[0];
					$vilBiblio = $row[1];
				}


$connect->close();

?>


<style type="text/css">
	.ui-datepicker-calendar {
		display: none;
	}
</style>

<!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="assests/plugins/fullcalendar/fullcalendar.min.css">
    <link rel="stylesheet" href="custom/css/carte.css"/>
    <link rel="stylesheet" href="assests/plugins/fullcalendar/fullcalendar.print.css" media="print">


<div class="row">
	
	<div class="col-md-4">
		<div class="panel panel-success">
			<div class="panel-heading">
				
				<a href="biblio.php" style="text-decoration:none;color:black;">
					Total Bibliothèque
					<span class="badge pull pull-right"><?php echo $countProduct; ?></span>	
				</a>
				
			</div> <!--/panel-hdeaing-->
		</div> <!--/panel-->
	</div> <!--/col-md-4-->

		<div class="col-md-4">
			<div class="panel panel-info">
			<div class="panel-heading">
				<a href="inscrit.php" style="text-decoration:none;color:black;">
					Nombre total des inscrits
					<span class="badge pull pull-right"><?php echo $countOrder; ?></span>
				</a>
					
			</div> <!--/panel-hdeaing-->
		</div> <!--/panel-->
		</div> <!--/col-md-4-->

	<div class="col-md-4">
		<div class="panel panel-danger">
			<div class="panel-heading">
				<a href="livre.php" style="text-decoration:none;color:black;">
					Stock total des livres
					<span class="badge pull pull-right"><?php echo $countLowStock; ?></span>	
				</a>
				
			</div> <!--/panel-hdeaing-->
		</div> <!--/panel-->
	</div> <!--/col-md-4-->

	
	<!--BLOC 1-->
	<div class="col-md-4">
		<div class="container card">
	<div class="row">
		<div class="">

            <div class="card hovercard">
                <div class="cardheader">

                </div>
                <div class="avatar">
                    <img alt="" src=<?php echo "Etape/",$urlPhoto;?>>
                </div>
                <div class="info">
                    <div class="title">
                        <a href="setting.php"><?php echo $nom," ",$prenom;?></a>
                    </div>
                    <div class="desc"><b><?php echo $fonction ?></b></div>
                    <div class="desc">Contact: <?php echo $contact ?></div>
                    <div class="desc">Email: <?php echo $mail ?></div>
                </div>
            </div>

        </div>

	</div>
</div>
		<br/>

		<div class="card">
		  <div class="cardHeader" style="background-color:#aa0000;">
		    <h2><?php echo $nomBiblio;?></h2>
		    <h3>(<?php echo $vilBiblio;?>)</h3>
		  </div>

		  <div class="cardContainer">
		    <p> <i class="glyphicon glyphicon-"></i>Bibliothèque</p>
		  </div>
		</div>
		<br/>

	</div>
	
	<!--Calendrier-->
<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading"> <i class="glyphicon glyphicon-calendar"></i> Calendrier</div>
			<div class="panel-body">
				<div id="calendar"></div>
			</div>	
		</div>
		
	</div>
	
</div> <!--/row-->

<!-- fullCalendar 2.2.5 -->
<script src="assests/plugins/moment/moment.min.js"></script>
<script src="assests/plugins/fullcalendar/fullcalendar.min.js"></script>


<script type="text/javascript">
	$(function () {
			// top bar active
	$('#navDashboard').addClass('active');

      //Date for the calendar events (dummy data)
      var date = new Date();
      var d = date.getDate(),
      m = date.getMonth(),
      y = date.getFullYear();

      $('#calendar').fullCalendar({
        header: {
          left: '',
          center: 'title'
        },
        buttonText: {
          today: 'today',
          month: 'month'          
        }        
      });


    });
</script>

<?php require_once 'includes/footer.php'; ?>