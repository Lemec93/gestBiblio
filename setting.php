<?php require_once 'includes/header.php'; ?>

<?php 
$id_user = $_SESSION['userId'];
$sql = "SELECT * FROM user WHERE id_user = {$id_user}";
$query = $connect->query($sql);
$result = $query->fetch_assoc();

$connect->close();
?>

<div class="row">
	<div class="col-md-12">
		<ol class="breadcrumb">
		  <li><a href="dashboard.php">Accueil</a></li>		  
		  <li class="active">Option</li>
		</ol>

		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="page-heading"> <i class="glyphicon glyphicon-wrench"></i> Option</div>
			</div> <!-- /panel-heading -->

			<div class="panel-body">

				<form action="php_action/changePhoto.php" method="post" class="form-horizontal" id="changePhotoUser"  enctype="multipart/form-data">
					<fieldset>
						<legend>Photo</legend>

						<div class="changePhotoUserMessages"></div>			

						<div class="form-group">
			        	<label for="getRespoImage" class="col-sm-3 control-label">Photo Actuelle: </label>
			        	<label class="col-sm-1 control-label">: </label>
						    <div class="col-sm-8">

						    <img src="Etape/<?php echo $result['pho_user'] ?>"  id="getRespoImage" class="thumbnail" style="width:250px; height:200px;" />
						    						
							    <div class="kv-avatar center-block form-group">					        
							        <input type="file" class="form-control" id="editPhoUser" name="editPhoUser" style="width:auto;" />
							    </div>
						      
						    </div>
			        </div>

					  <div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10">
					    	<input type="hidden" name="id_user" id="id_user" value="<?php echo $result['id_user'] ?>" /> 
					      <button type="submit" class="btn btn-success" data-loading-text="Loading..." id="changeUserPhotoBtn"> <i class="glyphicon glyphicon-ok-sign"></i> Modifier la photo</button>
					    </div>
					  </div>

					</fieldset>
				</form>				

				<form action="php_action/changeUsername.php" method="post" class="form-horizontal" id="changeUsernameForm">
					<fieldset>
						<legend>Option</legend>

						<div class="changeUsenrameMessages"></div>			

						<div class="form-group">
					    <label for="nom_user" class="col-sm-3 control-label">Nom</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="nom_user" name="nom_user" placeholder="Modifiez votre nom" value="<?php echo $result['nom_user']; ?>"/>
					    </div>
					  </div>

					  <div class="form-group">
					    <label for="pre_user" class="col-sm-3 control-label">Prénoms</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="pre_user" name="pre_user" placeholder="Modifiez votre Prénoms" value="<?php echo $result['pre_user']; ?>"/>
					    </div>
					  </div>

					  <div class="form-group">
					    <label for="tel_user" class="col-sm-3 control-label">Contact</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="tel_user" name="tel_user" placeholder="Modifiez votre Contact" value="<?php echo $result['tel_user']; ?>"/>
					    </div>
					  </div>

					  <div class="form-group">
					    <label for="eml_user" class="col-sm-3 control-label">Identifiant</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="eml_user" name="eml_user" placeholder="Modifiez votre Identifiant" value="<?php echo $result['eml_user']; ?>"/>
					    </div>
					  </div>

					  <div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10">
					    	<input type="hidden" name="id_user" id="id_user" value="<?php echo $result['id_user'] ?>" /> 
					      <button type="submit" class="btn btn-success" data-loading-text="Loading..." id="changeUsernameBtn"> <i class="glyphicon glyphicon-ok-sign"></i> Modifier les données</button>
					    </div>
					  </div>
					</fieldset>
				</form>

				<form action="php_action/changePassword.php" method="post" class="form-horizontal" id="changePasswordForm">
					<fieldset>
						<legend>Change le mot de passe</legend>

						<div class="changePasswordMessages"></div>

						<div class="form-group">
					    <label for="password" class="col-sm-3 control-label">Ancien mot de passe</label>
					    <div class="col-sm-9">
					      <input type="password" class="form-control" id="password" name="password" placeholder="Current Password">
					    </div>
					  </div>

					  <div class="form-group">
					    <label for="npassword" class="col-sm-3 control-label">Nouveau mot de passe</label>
					    <div class="col-sm-9">
					      <input type="password" class="form-control" id="npassword" name="npassword" placeholder="New Password">
					    </div>
					  </div>

					  <div class="form-group">
					    <label for="cpassword" class="col-sm-3 control-label">Mot de passe de confirmation</label>
					    <div class="col-sm-9">
					      <input type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Confirm Password">
					    </div>
					  </div>

					  <div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10">
					    	<input type="hidden" name="id_user" id="id_user" value="<?php echo $result['id_user'] ?>" /> 
					      <button type="submit" class="btn btn-primary"> <i class="glyphicon glyphicon-ok-sign"></i> Changer votre Mot de Passe </button>
					      
					    </div>
					  </div>


					</fieldset>
				</form>

			</div> <!-- /panel-body -->		

		</div> <!-- /panel -->		
	</div> <!-- /col-md-12 -->	
</div> <!-- /row-->


<script src="custom/js/setting.js"></script>
<?php require_once 'includes/footer.php'; ?>