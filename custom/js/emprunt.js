var manageEmprTable;
$(document).ready(function() {
	// top nav bar 
	$('#navEmprunt').addClass('active');
	// manage product data table
	manageEmprTable = $('#manageEmprTable').DataTable({
		'ajax': 'php_action/fetchEmpr.php',
		'order': []
	});

	// add product modal btn clicked
	$("#addEmprModalBtn").unbind('click').bind('click', function() {
		// // product form reset
		//$("#submitEmprForm")[0].reset();		

		// remove text-error 
		$(".text-danger").remove();
		// remove from-group error
		$(".form-group").removeClass('has-error').removeClass('has-success');

		// submit product form
		$("#submitEmprForm").unbind('submit').bind('submit', function() {


			// form validation
			var id_user   = $("#slt_insc").val();
			var id_livre  = $("#slt_livre").val();
			var dat_ret   = $("#dat_ret").val();
			
			if(id_user == "") {
				$("#slt_insc").after('<p class="text-danger">Le champ nom est obligatoire</p>');
				$('#slt_insc').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#slt_insc").find('.text-danger').remove();
				// id_user out for form 
				$("#slt_insc").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(id_livre == "") {
				$("#slt_livre").after('<p class="text-danger">Le champ prénoms est obligatoire</p>');
				$('#slt_livre').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#slt_livre").find('.text-danger').remove();
				// success out for form 
				$("#slt_livre").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(dat_ret == "") {
				$("#dat_ret").after('<p class="text-danger">Le champ contact est obligatoire</p>');
				$('#dat_ret').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#dat_ret").find('.text-danger').remove();
				// success out for form 
				$("#dat_ret").closest('.form-group').addClass('has-success');	  	
			}	// /else*/

			if(id_user && id_livre && dat_ret) {
				// submit loading button
				$("#createEmprBtn").button('loading');

				var form = $(this);
				var formData = new FormData(this);

				$.ajax({
					url : form.attr('action'),
					type: form.attr('method'),
					data: formData,
					dataType: 'json',
					cache: false,
					contentType: false,
					processData: false,
					success:function(response) {

						if(response.success == true) {
							// submit loading button
							$("#createEmprBtn").button('reset');
							
							$("#submitEmprForm")[0].reset();

							$("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);
																	
							// shows a successful message after operation
							$('#add-empr-messages').html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

							// remove the mesages
		          $(".alert-success").delay(500).show(10, function() {
								$(this).delay(3000).hide(10, function() {
									$(this).remove();
								});
							}); // /.alert

		          // reload the manage student table
							manageEmprTable.ajax.reload(null, true);

							// remove text-error 
							$(".text-danger").remove();
							// remove from-group error
							$(".form-group").removeClass('has-error').removeClass('has-success');

						} // /if response.success
						
					} // /success function
				}); // /ajax function
			}	 // /if validation is ok 					

			return false;
		}); // /submit product form

	}); // /add product modal btn clicked */
	

	// remove product 	

}); // document.ready fucntion 

function editEmpr(id_empr = null) {

	if(id_empr) {
		
		//$("#id_empr").remove();		
		// remove text-error 
		$(".text-danger").remove();
		// remove from-group error
		$(".form-group").removeClass('has-error').removeClass('has-success');
		// modal spinner
		$('.div-loading').removeClass('div-hide');
		// modal div
		$('.div-result').addClass('div-hide');
//alert("AAA");

		$.ajax({
			url: 'php_action/fetchSelectedEmpr.php',
			type: 'post',
			data: {id_empr: id_empr},
			dataType: 'json',
			success:function(response) {		
			// alert(response.product_image);

				// modal spinner
				$('.div-loading').addClass('div-hide');
				// modal div
				//$('.div-result').removeClass('div-hide');				

				// product id 
				$(".editEmprFooter").append('<input type="hidden" name="id_empr" id="id_empr" value="'+response.id_tach+'" />');				
				

				// product name
				$("#edit_slt_insc").val(response.id_user);
				// quantity
				$("#edit_slt_livre").val(response.id_livre);
				// rate
				$("#edit_dat_ret").val(response.dat_ret);
				// brand name

				// update the product data function
				$("#editEmprForm").unbind('submit').bind('submit', function() {

					// form validation
					var id_user = $("#edit_slt_insc").val();
					var id_livre = $("#edit_slt_livre").val();
					var dat_ret = $("#edit_dat_ret").val();
					//var productStatus = $("#editProductStatus").val();
							

					if(id_user == "") {
						$("#edit_slt_insc").after('<p class="text-danger">Product Name field is required</p>');
						$('#edit_slt_insc').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#edit_slt_insc").find('.text-danger').remove();
						// success out for form 
						$("#edit_slt_insc").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(id_livre == "") {
						$("#edit_slt_livre").after('<p class="text-danger">Quantity field is required</p>');
						$('#edit_slt_livre').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#edit_slt_livre").find('.text-danger').remove();
						// success out for form 
						$("#edit_slt_livre").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(dat_ret == "") {
						$("#edit_dat_ret").after('<p class="text-danger">Rate field is required</p>');
						$('#edit_dat_ret').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#edit_dat_ret").find('.text-danger').remove();
						// success out for form 
						$("#edit_dat_ret").closest('.form-group').addClass('has-success');	  	
					}	// /else
// /else

					
					if(id_user && id_livre && dat_ret) {
						// submit loading button
						$("#editEmprBtn").button('loading');
	//
						var form = $(this);
						//var formData = new FormData(this);

						$.ajax({
							url : form.attr('action'),
							type: form.attr('method'),
							data: form.serialize(),
							dataType: 'json',
							success:function(response) {
								//console.log(response);
								//alert(response.success);
								if(response.success == true) {
									// submit loading button
									$("#editEmprBtn").button('reset');																		

									$("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);
																			
									// shows a successful message after operation
									$('#edit-empr-messages').html('<div class="alert alert-success">'+
				            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
				            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
				          '</div>');

									// remove the mesages
				          $(".alert-success").delay(500).show(10, function() {
										$(this).delay(3000).hide(10, function() {
											$(this).remove();
										});
									}); // /.alert

				          // reload the manage student table
									manageEmprTable.ajax.reload(null, true);

									// remove text-error 
									$(".text-danger").remove();
									// remove from-group error
									$(".form-group").removeClass('has-error').removeClass('has-success');

								} // /if response.success
								
							} // /success function
						}); // /ajax function
					}	 // /if validation is ok 					

					return false;
				}); // update the product data function

			} // /success function
		}); // /ajax to fetch product image

				
	} else {
		alert('error please refresh the page');
	}
} // /edit product function */

// remove product 
function removeEmpr(id_empr = null) {
	if(id_empr) {
		// remove product button clicked
		$("#removeEmprBtn").unbind('click').bind('click', function() {
			// loading remove button
			$("#removeEmprBtn").button('loading');
			$.ajax({
				url: 'php_action/removeEmpr.php',
				type: 'post',
				data: {id_empr: id_empr},
				dataType: 'json',
				success:function(response) {
					// loading remove button
					$("#removeEmprBtn").button('reset');
					if(response.success == true) {
						// remove product modal
						$("#removeEmprModal").modal('hide');

						// update the product table
						manageEmprTable.ajax.reload(null, false);

						// remove success messages
						$(".remove-messages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						// remove the mesages
	          $(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert
					} else {

						// remove success messages
						$(".removeRespoMessages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						// remove the mesages
	          $(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert

					} // /error
				} // /success function manageRespoTable
			}); // /ajax fucntion to remove the product
			return false;
		}); // /remove product btn clicked
	} // /if productid
} // /remove product function

/*function clearForm(oForm) {
	// var frm_elements = oForm.elements;									
	// console.log(frm_elements);
	// 	for(i=0;i<frm_elements.length;i++) {
	// 		field_type = frm_elements[i].type.toLowerCase();									
	// 		switch (field_type) {
	// 	    case "text":
	// 	    case "password":
	// 	    case "textarea":
	// 	    case "hidden":
	// 	    case "select-one":	    
	// 	      frm_elements[i].value = "";
	// 	      break;
	// 	    case "radio":
	// 	    case "checkbox":	    
	// 	      if (frm_elements[i].checked)
	// 	      {
	// 	          frm_elements[i].checked = false;
	// 	      }
	// 	      break;
	// 	    case "file": 
	// 	    	if(frm_elements[i].options) {
	// 	    		frm_elements[i].options= false;
	// 	    	}
	// 	    default:
	// 	        break;
	//     } // /switch
	// 	} // for
}*/