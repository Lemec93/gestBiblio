var manageLivreUserTable;

$(document).ready(function() {
	// top nav bar 
	$('#navLivreUser').addClass('active');
	// manage product data table
	manageLivreUserTable = $('#manageLivreUserTable').DataTable({
		'ajax': 'php_action/fetchLivreUser.php',
		'order': []
	});

}); // document.ready fucntion

function affichage(id_livre = null){
	
	if(id_livre) {
		$("#id_livre").remove();		
		//alert(id_livre);
		$.ajax({
				url: 'php_action/fetchSelectedLivreUser.php',
				type: 'post',
				data: {id_livre: id_livre},
				dataType: 'json',
				success:function(response) {
					//alert(response.tit_livre);
					document.getElementById("tit").innerHTML = response.tit_livre;
					document.getElementById("aut").innerHTML = response.aut_livre;
					document.getElementById("gen").innerHTML = response.gen_livre;
					document.getElementById("lan").innerHTML = response.lan_livre;
					document.getElementById("edi").innerHTML = response.edi_livre;
					document.getElementById("res").innerHTML = response.res_livre;
					//document.getElementsByTagName("img")[0].setAttribute("src", response.img_livre);
					document.getElementById("imgliv").setAttribute("src", "--/"+response.img_livre);
				}
			});
	}
}

/*function createRes(id_livre = null) {
	if(id_livre) {{
		// remove product button clicked
		$("#createResBtn").unbind('click').bind('click', function() {
			// loading remove button
			$("#createResBtn").button('loading');
			$.ajax({
				url: 'php_action/createRes.php',
				type: 'post',
				data: {id_livre: id_livre},
				dataType: 'json',
				success:function(response) 
					// loading remove button
					$("#createResBtn").button('reset');
					if(response.success == true) {
						// remove product modal
						$("#removeResModal").modal('hide');

						// update the product table
						manageTable.ajax.reload(null, false);

						// remove success messages
						$(".remove-messages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						// remove the mesages
	          $(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert
					} else {

						// remove success messages
						$(".removeRespoMessages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						// remove the mesages
	          $(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert

					} // /error
				} // /success function manageRespoTable
			}); // /ajax fucntion to remove the product
			return false;
		}); // /remove product btn clicked
	} // /if productid
}*/
