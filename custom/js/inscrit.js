var manageInscritTable;

$(document).ready(function() {
	// top nav bar 
	$('#navInscrit').addClass('active');
	// manage product data table
	manageInscritTable = $('#manageInscritTable').DataTable({
		'ajax': 'php_action/fetchInscrit.php',
		'order': []
	});

	// add product modal btn clicked
	/*$("#addRespoModalBtn").unbind('click').bind('click', function() {
		// // product form reset
		$("#submitRespoForm")[0].reset();		

		// remove text-error 
		$(".text-danger").remove();
		// remove from-group error
		$(".form-group").removeClass('has-error').removeClass('has-success');

		$("#respoImage").fileinput({
	      overwriteInitial: true,
		    maxFileSize: 2500,
		    showClose: false,
		    showCaption: false,
		    browseLabel: '',
		    removeLabel: '',
		    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
		    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
		    removeTitle: 'Cancel or reset changes',
		    elErrorContainer: '#kv-avatar-errors-1',
		    msgErrorClass: 'alert alert-block alert-danger',
		    defaultPreviewContent: '<img src="assests/images/photo_default.png" alt="Profile Image" style="width:100%;">',
		    layoutTemplates: {main2: '{preview} {remove} {browse}'},								    
	  		allowedFileExtensions: ["jpg", "png", "gif", "JPG", "PNG", "GIF"]
			});   

		// submit product form
		$("#submitRespoForm").unbind('submit').bind('submit', function() {

			// form validation
			//var respoImage = $("#respoImage").val();
			var nom_respo  = $("#nom_respo").val();
			var pre_respo  = $("#pre_respo").val();
			var tel_respo  = $("#tel_respo").val();
			var eml_respo  = $("#eml_respo").val();
			//var pwd_respo  = $("#pwd_respo").val();
			var slt_biblio = $("#slt_biblio").val();
	
			/*if(respoImage == "") {
				$("#respoImage").closest('.center-block').after('<p class="text-danger">Photo profil field is required</p>');
				$('#respoImage').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#respoImage").find('.text-danger').remove();
				// success out for form 
				$("#respoImage").closest('.form-group').addClass('has-success');	  	
			}	// /else*/

			/*if(nom_respo == "") {
				$("#nom_respo").after('<p class="text-danger">Le champ nom est obligatoire</p>');
				$('#nom_respo').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#nom_respo").find('.text-danger').remove();
				// success out for form 
				$("#nom_respo").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(pre_respo == "") {
				$("#pre_respo").after('<p class="text-danger">Le champ prénoms est obligatoire</p>');
				$('#pre_respo').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#pre_respo").find('.text-danger').remove();
				// success out for form 
				$("#pre_respo").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(tel_respo == "") {
				$("#tel_respo").after('<p class="text-danger">Le champ contact est obligatoire</p>');
				$('#tel_respo').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#tel_respo").find('.text-danger').remove();
				// success out for form 
				$("#tel_respo").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(eml_respo == "") {
				$("#eml_respo").after('<p class="text-danger">Le champ email est obligatoire</p>');
				$('#eml_respo').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#eml_respo").find('.text-danger').remove();
				// success out for form 
				$("#eml_respo").closest('.form-group').addClass('has-success');	  	
			}	// /else

			/*if(pwd_respo == "") {
				$("#pwd_respo").after('<p class="text-danger">La selection bibliothèque est obligatoire</p>');
				$('#pwd_respo').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#pwd_respo").find('.text-danger').remove();
				// success out for form 
				$("#pwd_respo").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(slt_biblio == "") {
				$("#slt_biblio").after('<p class="text-danger">La selection bibliothèque est obligatoire</p>');
				$('#slt_biblio').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#slt_biblio").find('.text-danger').remove();
				// success out for form 
				$("#slt_biblio").closest('.form-group').addClass('has-success');	  	
			}	// /else

			/*if(productStatus == "") {
				$("#productStatus").after('<p class="text-danger">Product Status field is required</p>');
				$('#productStatus').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#productStatus").find('.text-danger').remove();
				// success out for form 
				$("#productStatus").closest('.form-group').addClass('has-success');	  	
			}	// /elsex

			if(nom_respo && pre_respo && tel_respo && eml_respo && slt_biblio) {
				// submit loading button
				$("#createRespoBtn").button('loading');

				var form = $(this);
				var formData = new FormData(this);

				$.ajax({
					url : form.attr('action'),
					type: form.attr('method'),
					data: formData,
					dataType: 'json',
					cache: false,
					contentType: false,
					processData: false,
					success:function(response) {

						if(response.success == true) {
							// submit loading button
							$("#createRespoBtn").button('reset');
							
							$("#submitRespoForm")[0].reset();

							$("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);
																	
							// shows a successful message after operation
							$('#add-respo-messages').html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

							// remove the mesages
		          $(".alert-success").delay(500).show(10, function() {
								$(this).delay(3000).hide(10, function() {
									$(this).remove();
								});
							}); // /.alert

		          // reload the manage student table
							manageRespoTable.ajax.reload(null, true);

							// remove text-error 
							$(".text-danger").remove();
							// remove from-group error
							$(".form-group").removeClass('has-error').removeClass('has-success');

						} // /if response.success
						
					} // /success function
				}); // /ajax function
			}	 // /if validation is ok 					

			return false;
		}); // /submit product form

	}); // /add product modal btn clicked */
	

	// remove product 	

}); // document.ready fucntion

/*function editRespo(id_respo = null) {

	if(id_respo) {
		$("#id_respo").remove();		
		// remove text-error 
		$(".text-danger").remove();
		// remove from-group error
		$(".form-group").removeClass('has-error').removeClass('has-success');
		// modal spinner
		$('.div-loading').removeClass('div-hide');
		// modal div
		$('.div-result').addClass('div-hide');

		$.ajax({
			url: 'php_action/fetchSelectedRespo.php',
			type: 'post',
			data: {id_respo: id_respo},
			dataType: 'json',
			success:function(response) {		
			// alert(response.product_image);
				// modal spinner
				$('.div-loading').addClass('div-hide');
				// modal div
				$('.div-result').removeClass('div-hide');				

				$("#getRespoImage").attr('src', 'stock/'+response.respo_image);

				$("#editRespoImage").fileinput({		      
				});  

				// $("#editProductImage").fileinput({
		  //     overwriteInitial: true,
			 //    maxFileSize: 2500,
			 //    showClose: false,
			 //    showCaption: false,
			 //    browseLabel: '',
			 //    removeLabel: '',
			 //    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
			 //    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
			 //    removeTitle: 'Cancel or reset changes',
			 //    elErrorContainer: '#kv-avatar-errors-1',
			 //    msgErrorClass: 'alert alert-block alert-danger',
			 //    defaultPreviewContent: '<img src="stock/'+response.product_image+'" alt="Profile Image" style="width:100%;">',
			 //    layoutTemplates: {main2: '{preview} {remove} {browse}'},								    
		  // 		allowedFileExtensions: ["jpg", "png", "gif", "JPG", "PNG", "GIF"]
				// });  

				// product id 
				$(".editRespoFooter").append('<input type="hidden" name="id_respo" id="id_respo" value="'+response.id_respo+'" />');				
				$(".editRespoPhotoFooter").append('<input type="hidden" name="id_respo" id="id_respo" value="'+response.id_respo+'" />');				
				
				// product name
				$("#editNomRespo").val(response.nom_respo);
				// quantity
				$("#editPreRespo").val(response.pre_respo);
				// rate
				$("#editTelRespo").val(response.tel_respo);
				// brand name
				$("#editEmlRespo").val(response.eml_respo);
				// category name
				$("#editPwdRespo").val(response.pwd_respo);
				// status
				//$("#editIdRespo").val(response.id);

				// update the product data function
				$("#editRespoForm").unbind('submit').bind('submit', function() {

					// form validation
					var respoImage = $("#editRespoImage").val();
					var nom_respo = $("#editNomRespo").val();
					var pre_respo = $("#editPreRespo").val();
					var tel_respo = $("#editTelRespo").val();
					var eml_respo = $("#editEmlRespo").val();
					var pwd_respo = $("#editPwdRespo").val();
					var slt_biblio = $("#editSltBiblio").val();
					//var productStatus = $("#editProductStatus").val();
								

					if(nom_respo == "") {
						$("#editNomRespo").after('<p class="text-danger">Product Name field is required</p>');
						$('#editNomRespo').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editNomRespo").find('.text-danger').remove();
						// success out for form 
						$("#editNomRespo").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(pre_respo == "") {
						$("#editPreRespo").after('<p class="text-danger">Quantity field is required</p>');
						$('#editPreRespo').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editPreRespo").find('.text-danger').remove();
						// success out for form 
						$("#editPreRespo").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(tel_respo == "") {
						$("#editTelRespo").after('<p class="text-danger">Rate field is required</p>');
						$('#editTelRespo').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editTelRespo").find('.text-danger').remove();
						// success out for form 
						$("#editTelRespo").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(eml_respo == "") {
						$("#editEmlRespo").after('<p class="text-danger">Brand Name field is required</p>');
						$('#editEmlRespo').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editEmlRespo").find('.text-danger').remove();
						// success out for form 
						$("#editEmlRespo").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(pwd_respo == "") {
						$("#editPwdRespo").after('<p class="text-danger">Category Name field is required</p>');
						$('#editPwdRespo').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editPwdRespo").find('.text-danger').remove();
						// success out for form 
						$("#editPwdRespo").closest('.form-group').addClass('has-success');	  	
					}	// /else


					if(slt_biblio == "") {
						$("#editSltBiblio").after('<p class="text-danger">Category Name field is required</p>');
						$('#editSltBiblio').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editSltBiblio").find('.text-danger').remove();
						// success out for form 
						$("#editSltBiblio").closest('.form-group').addClass('has-success');	  	
					}	// /else
					
					if(nom_respo && pre_respo && tel_respo && eml_respo && pwd_respo && slt_biblio) {
						// submit loading button
						$("#editRespoBtn").button('loading');

						var form = $(this);
						var formData = new FormData(this);

						$.ajax({
							url : form.attr('action'),
							type: form.attr('method'),
							data: formData,
							dataType: 'json',
							cache: false,
							contentType: false,
							processData: false,
							success:function(response) {
								console.log(response);
								if(response.success == true) {
									// submit loading button
									$("#editRespoBtn").button('reset');																		

									$("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);
																			
									// shows a successful message after operation
									$('#edit-respo-messages').html('<div class="alert alert-success">'+
				            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
				            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
				          '</div>');

									// remove the mesages
				          $(".alert-success").delay(500).show(10, function() {
										$(this).delay(3000).hide(10, function() {
											$(this).remove();
										});
									}); // /.alert

				          // reload the manage student table
									manageRespoTable.ajax.reload(null, true);

									// remove text-error 
									$(".text-danger").remove();
									// remove from-group error
									$(".form-group").removeClass('has-error').removeClass('has-success');

								} // /if response.success
								
							} // /success function
						}); // /ajax function
					}	 // /if validation is ok 					

					return false;
				}); // update the product data function

				// update the product image				
				$("#updateRespoImageForm").unbind('submit').bind('submit', function() {					
					// form validation
					var respoImage = $("#editRespoImage").val();					
					
					if(respoImage == "") {
						$("#editRespoImage").closest('.center-block').after('<p class="text-danger">Product Image field is required</p>');
						$('#editRespoImage').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editRespoImage").find('.text-danger').remove();
						// success out for form 
						$("#editRespoImage").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(respoImage) {
						// submit loading button
						$("#editRespoImageBtn").button('loading');

						var form = $(this);
						var formData = new FormData(this);

						$.ajax({
							url : form.attr('action'),
							type: form.attr('method'),
							data: formData,
							dataType: 'json',
							cache: false,
							contentType: false,
							processData: false,
							success:function(response) {
								
								if(response.success == true) {
									// submit loading button
									$("#editRespoImageBtn").button('reset');																		

									$("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);
																			
									// shows a successful message after operation
									$('#edit-respoPhoto-messages').html('<div class="alert alert-success">'+
				            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
				            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
				          '</div>');

									// remove the mesages
				          $(".alert-success").delay(500).show(10, function() {
										$(this).delay(3000).hide(10, function() {
											$(this).remove();
										});
									}); // /.alert

				          // reload the manage student table
									manageRespoTable.ajax.reload(null, true);

									$(".fileinput-remove-button").click();

									$.ajax({
										url: 'php_action/fetchRespoImageUrl.php?i='+id_respo,
										type: 'post',
										success:function(response) {
										$("#getRespoImage").attr('src', response);		
										}
									});																		

									// remove text-error 
									$(".text-danger").remove();
									// remove from-group error
									$(".form-group").removeClass('has-error').removeClass('has-success');

								} // /if response.success
								
							} // /success function
						}); // /ajax function
					}	 // /if validation is ok 					

					return false;
				}); // /update the product image

			} // /success function
		}); // /ajax to fetch product image

				
	} else {
		alert('error please refresh the page');
	}
} // /edit product function */

// remove product 
/*function removeRespo(id_respo = null) {
	if(id_respo) {
		// remove product button clicked
		$("#removeRespoBtn").unbind('click').bind('click', function() {
			// loading remove button
			$("#removeRespoBtn").button('loading');
			$.ajax({
				url: 'php_action/removeRespo.php',
				type: 'post',
				data: {id_respo: id_respo},
				dataType: 'json',
				success:function(response) {
					// loading remove button
					$("#removeRespoBtn").button('reset');
					if(response.success == true) {
						// remove product modal
						$("#removeRespoModal").modal('hide');

						// update the product table
						manageRespoTable.ajax.reload(null, false);

						// remove success messages
						$(".remove-messages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						// remove the mesages
	          $(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert
					} else {

						// remove success messages
						$(".removeRespoMessages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						// remove the mesages
	          $(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert

					} // /error
				} // /success function manageRespoTable
			}); // /ajax fucntion to remove the product
			return false;
		}); // /remove product btn clicked
	} // /if productid
} // /remove product function*/

/*function clearForm(oForm) {
	// var frm_elements = oForm.elements;									
	// console.log(frm_elements);
	// 	for(i=0;i<frm_elements.length;i++) {
	// 		field_type = frm_elements[i].type.toLowerCase();									
	// 		switch (field_type) {
	// 	    case "text":
	// 	    case "password":
	// 	    case "textarea":
	// 	    case "hidden":
	// 	    case "select-one":	    
	// 	      frm_elements[i].value = "";
	// 	      break;
	// 	    case "radio":
	// 	    case "checkbox":	    
	// 	      if (frm_elements[i].checked)
	// 	      {
	// 	          frm_elements[i].checked = false;
	// 	      }
	// 	      break;
	// 	    case "file": 
	// 	    	if(frm_elements[i].options) {
	// 	    		frm_elements[i].options= false;
	// 	    	}
	// 	    default:
	// 	        break;
	//     } // /switch
	// 	} // for
}*/