var manageBiblioTable;

$(document).ready(function() {
	// top bar active
	$('#navBiblio').addClass('active');
	
	// manage biblio table
	manageBiblioTable = $("#manageBiblioTable").DataTable({
		'ajax': 'php_action/fetchBiblio.php',
		'order': []		
	});

	// submit brand form function
	$("#submitBiblioForm").unbind('submit').bind('submit', function() {
		// remove the error text
		$(".text-danger").remove();
		// remove the form error
		$('.form-group').removeClass('has-error').removeClass('has-success');			

		var nomBiblio = $("#nomBiblio").val();
		var vilBiblio = $("#vilBiblio").val();
		var staBiblio = $("#staBiblio").val();

		if(nomBiblio == "") {
			$("#nomBiblio").after('<p class="text-danger">Le nom de la Bibliothèque est nécessaire</p>');
			$('#nomBiblio').closest('.form-group').addClass('has-error');
		} else {
			// remov error text field
			$("#nomBiblio").find('.text-danger').remove();
			// success out for form 
			$("#nomBiblio").closest('.form-group').addClass('has-success');	  	
		}

		if(vilBiblio == "") {
			$("#vilBiblio").after('<p class="text-danger">La ville de la Bibliothèque est nécessaire</p>');
			$('#vilBiblio').closest('.form-group').addClass('has-error');
		} else {
			// remov error text field
			$("#vilBiblio").find('.text-danger').remove();
			// success out for form 
			$("#vilBiblio").closest('.form-group').addClass('has-success');	  	
		}

		if(staBiblio == "") {
			$("#staBiblio").after('<p class="text-danger">Le status de la Bibliothèque est nécessaire</p>');

			$('#staBiblio').closest('.form-group').addClass('has-error');
		} else {
			// remov error text field
			$("#staBiblio").find('.text-danger').remove();
			// success out for form 
			$("#staBiblio").closest('.form-group').addClass('has-success');	  	
		}

		if(nomBiblio && vilBiblio && staBiblio) {
			var form = $(this);
			// button loading
			$("#createBiblioBtn").button('loading');

			$.ajax({
				url : form.attr('action'),
				type: form.attr('method'),
				data: form.serialize(),
				dataType: 'json',
				success:function(response) {
					// button loading
					$("#createBiblioBtn").button('reset');

					if(response.success == true) {
						// reload the manage member table 
						manageBiblioTable.ajax.reload(null, false);						

  	  			// reset the form text
						$("#submitBiblioForm")[0].reset();
						// remove the error text
						$(".text-danger").remove();
						// remove the form error
						$('.form-group').removeClass('has-error').removeClass('has-success');
  	  			
  	  			$('#add-biblio-messages').html('<div class="alert alert-success">'+
            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
          '</div>');

  	  			$(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert
					}  // if

				} // /success
			}); // /ajax	
		} // if

		return false;
	}); // /submit biblio form function 

});






function editBiblio(idBiblio = null) {
	
	if(idBiblio) {
		// remove hidden biblio id text
		$('#idBiblio').remove();

		// remove the error 
		$('.text-danger').remove();
		// remove the form-error
		$('.form-group').removeClass('has-error').removeClass('has-success');

		// modal loading
		$('.modal-loading').removeClass('div-hide');
		// modal result
		$('.edit-biblio-result').addClass('div-hide');
		// modal footer
		$('.editBiblioFooter').addClass('div-hide');

		$.ajax({
			url: 'php_action/fetchSelectedBiblio.php',
			type: 'post',
			data: {idBiblio : idBiblio},
			dataType: 'json',
			success:function(response) {
				// modal loading
				$('.modal-loading').addClass('div-hide');
				// modal result
				$('.edit-biblio-result').removeClass('div-hide');
				// modal footer
				$('.editBiblioFooter').removeClass('div-hide');

				// setting the biblio name value 
				$('#editNomBiblio').val(response.nom_biblio);
				
				// setting the biblio town value 
				$('#editVilBiblio').val(response.vil_biblio);

				// setting the biblio status value
				$('#editStaBiblio').val(response.act_biblio);
				// biblio id 
				$(".editBiblioFooter").after('<input type="hidden" name="idBiblio" id="idBiblio" value="'+response.id_biblio+'" />');

				// update biblio form 
				$('#editBiblioForm').unbind('submit').bind('submit', function() {

					// remove the error text
					$(".text-danger").remove();
					// remove the form error
					$('.form-group').removeClass('has-error').removeClass('has-success');			

					var editNomBiblio = $('#editNomBiblio').val();
					var editVilBiblio = $('#editVilBiblio').val();
					var editStaBiblio = $('#editStaBiblio').val();

					if(editNomBiblio == "") {
						$("#editNomBiblio").after('<p class="text-danger">Le nom de la Bibliothèque est nécessaire</p>');
						$('#editNomBiblio').closest('.form-group').addClass('has-error');
					} else {
						// remov error text field
						$("#editNomBiblio").find('.text-danger').remove();
						// success out for form 
						$("#editNomBiblio").closest('.form-group').addClass('has-success');	  	
					}

					if(editVilBiblio == "") {
						$("#editVilBiblio").after('<p class="text-danger">La ville de la Bibliothèque est nécessaire</p>');
						$('#editVilBiblio').closest('.form-group').addClass('has-error');
					} else {
						// remov error text field
						$("#editVilBiblio").find('.text-danger').remove();
						// success out for form 
						$("#editVilBiblio").closest('.form-group').addClass('has-success');	  	
					}

					if(editStaBiblio == "") {
						$("#editStaBiblio").after('<p class="text-danger">Le status de la Bibliothèque est nécessaire</p>');

						$('#editStaBiblio').closest('.form-group').addClass('has-error');
					} else {
						// remove error text field
						$("#editStaBiblio").find('.text-danger').remove();
						// success out for form 
						$("#editStaBiblio").closest('.form-group').addClass('has-success');	  	
					}

					if(editNomBiblio && editVilBiblio && editStaBiblio) {
						var form = $(this);

						// submit btn
						$('#editBiblioBtn').button('loading');

						$.ajax({
							url: form.attr('action'),
							type: form.attr('method'),
							data: form.serialize(),
							dataType: 'json',
							success:function(response) {

								if(response.success == true) {
									console.log(response);
									// submit btn
									$('#editBiblioBtn').button('reset');

									// reload the manage member table 
									manageBiblioTable.ajax.reload(null, false);								  	  										
									// remove the error text
									$(".text-danger").remove();
									// remove the form error
									$('.form-group').removeClass('has-error').removeClass('has-success');
			  	  			
			  	  			$('#edit-biblio-messages').html('<div class="alert alert-success">'+
			            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
			            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
			          '</div>');

			  	  			$(".alert-success").delay(500).show(10, function() {
										$(this).delay(3000).hide(10, function() {
											$(this).remove();
										});
									}); // /.alert
								} // /if
									
							}// /success
						});	 // /ajax												
					} // /if

					return false;
				}); // /update brand form

			} // /success
		}); // ajax function

	} else {
		alert('Erreur!! Actualisez la page à nouveau');
	}
} // /edit biblio function






function removeBiblio(idBiblio = null) {
	if(idBiblio) {
		$('#removeIdBiblio').remove();
		$.ajax({
			url: 'php_action/fetchSelectedBiblio.php',
			type: 'post',
			data: {idBiblio : idBiblio},
			dataType: 'json',
			success:function(response) {
				$('.removeBiblioFooter').after('<input type="hidden" name="removeIdBiblio" id="removeIdBiblio" value="'+response.id_biblio+'" /> ');

				// click on remove button to remove the brand
				$("#removeBiblioBtn").unbind('click').bind('click', function() {
					// button loading
					$("#removeBiblioBtn").button('loading');

					$.ajax({
						url: 'php_action/removeBiblio.php',
						type: 'post',
						data: {idBiblio : idBiblio},
						dataType: 'json',
						success:function(response) {
							console.log(response);
							// button loading
							$("#removeBiblioBtn").button('reset');
							if(response.success == true) {

								// hide the remove modal 
								$('#removeMemberModal').modal('hide');

								// reload the biblio table 
								manageBiblioTable.ajax.reload(null, false);
								
								$('.remove-messages').html('<div class="alert alert-success">'+
			            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
			            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
			          '</div>');

			  	  			$(".alert-success").delay(500).show(10, function() {
										$(this).delay(3000).hide(10, function() {
											$(this).remove();
										});
									}); // /.alert
							} else {

							} // /else
						} // /response messages
					}); // /ajax function to remove the biblio

				}); // /click on remove button to remove the biblio

			} // /success
		}); // /ajax

		$('.removeBiblioFooter').after();
	} else {
		alert('Erreur!! Actualisez la page à nouveau');
	}
} // /remove biblio function
