var manageInscTable;

$(document).ready(function() {
	// top nav bar 
	$('#navInsc').addClass('active');
	// manage product data table
	manageInscTable = $('#manageInscTable').DataTable({
		'ajax': 'php_action/fetchInsc.php',
		'order': []
	});

	// add product modal btn clicked
	$("#addInscModalBtn").unbind('click').bind('click', function() {
		// // product form reset
		$("#submitInscForm")[0].reset();		

		// remove text-error 
		$(".text-danger").remove();
		// remove from-group error
		$(".form-group").removeClass('has-error').removeClass('has-success');

		/*$("#inscImage").fileinput({
	      overwriteInitial: true,
		    maxFileSize: 2500,
		    showClose: false,
		    showCaption: false,
		    browseLabel: '',
		    removeLabel: '',
		    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
		    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
		    removeTitle: 'Cancel or reset changes',
		    elErrorContainer: '#kv-avatar-errors-1',
		    msgErrorClass: 'alert alert-block alert-danger',
		    defaultPreviewContent: '<img src="assests/images/photo_default.png" alt="Profile Image" style="width:100%;">',
		    layoutTemplates: {main2: '{preview} {remove} {browse}'},								    
	  		allowedFileExtensions: ["jpg", "png", "gif", "JPG", "PNG", "GIF"]
			});  */ 

		// submit product form
		$("#submitInscForm").unbind('submit').bind('submit', function() {

			// form validation
			//var respoImage = $("#respoImage").val();
			var nom_insc  = $("#nom_insc").val();
			var pre_insc  = $("#pre_insc").val();
			//var det_insc  = $("#det_insc").val();
			var tel_insc  = $("#tel_insc").val();
			var eml_insc  = $("#eml_insc").val();
			var det_insc  = $("#det_insc").val();
			//var pwd_respo  = $("#pwd_respo").val();
			var slt_fonc = $("#slt_fonc").val();
			//var aa=2;
	
			/*if(respoImage == "") {
				$("#respoImage").closest('.center-block').after('<p class="text-danger">Photo profil field is required</p>');
				$('#respoImage').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#respoImage").find('.text-danger').remove();
				// success out for form 
				$("#respoImage").closest('.form-group').addClass('has-success');	  	
			}	// /else*/

			if(nom_insc == "") {
				$("#nom_insc").after('<p class="text-danger">Le champ nom est obligatoire</p>');
				$('#nom_insc').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#nom_insc").find('.text-danger').remove();
				// success out for form 
				$("#nom_insc").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(pre_insc == "") {
				$("#pre_insc").after('<p class="text-danger">Le champ prénoms est obligatoire</p>');
				$('#pre_insc').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#pre_insc").find('.text-danger').remove();
				// success out for form 
				$("#pre_insc").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(tel_insc == "") {
				$("#tel_insc").after('<p class="text-danger">Le champ contact est obligatoire</p>');
				$('#tel_insc').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#tel_insc").find('.text-danger').remove();
				// success out for form 
				$("#tel_insc").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(eml_insc == "") {
				$("#eml_insc").after('<p class="text-danger">Le champ email est obligatoire</p>');
				$('#eml_insc').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#eml_insc").find('.text-danger').remove();
				// success out for form 
				$("#eml_insc").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(det_insc == "") {
				$("#det_insc").after('<p class="text-danger">Le champ email est obligatoire</p>');
				$('#det_insc').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#det_insc").find('.text-danger').remove();
				// success out for form 
				$("#det_insc").closest('.form-group').addClass('has-success');	  	
			}	// /else

			/*if(pwd_respo == "") {
				$("#pwd_respo").after('<p class="text-danger">La selection bibliothèque est obligatoire</p>');
				$('#pwd_respo').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#pwd_respo").find('.text-danger').remove();
				// success out for form 
				$("#pwd_respo").closest('.form-group').addClass('has-success');	  	
			}	// /else*/

			if(slt_fonc == "") {
				$("#slt_foncslt_fonc").after('<p class="text-danger">La selection bibliothèque est obligatoire</p>');
				$('#slt_foncslt_fonc').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#slt_foncslt_fonc").find('.text-danger').remove();
				// success out for form 
				$("#slt_foncslt_fonc").closest('.form-group').addClass('has-success');	  	
			}	// /else

			/*if(productStatus == "") {
				$("#productStatus").after('<p class="text-danger">Product Status field is required</p>');
				$('#productStatus').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#productStatus").find('.text-danger').remove();
				// success out for form 
				$("#productStatus").closest('.form-group').addClass('has-success');	  	
			}	// /else*/

			if(nom_insc && pre_insc && tel_insc && eml_insc && slt_fonc && det_insc) {
				// submit loading button
				$("#createInscBtn").button('loading');

				var form = $(this);
				var formData = new FormData(this);

				$.ajax({
					url : form.attr('action'),
					type: form.attr('method'),
					data: formData,
					dataType: 'json',
					cache: false,
					contentType: false,
					processData: false,
					success:function(response) {

						if(response.success == true) {
							// submit loading button
							$("#createInscBtn").button('reset');
							
							$("#submitInscForm")[0].reset();

							$("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);
																	
							// shows a successful message after operation
							$('#add-insc-messages').html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

							// remove the mesages
		          $(".alert-success").delay(500).show(10, function() {
								$(this).delay(3000).hide(10, function() {
									$(this).remove();
								});
							}); // /.alert

		          // reload the manage student table
							manageInscTable.ajax.reload(null, true);

							// remove text-error 
							$(".text-danger").remove();
							// remove from-group error
							$(".form-group").removeClass('has-error').removeClass('has-success');

						} // /if response.success
						
					} // /success function
				}); // /ajax function
			}	 // /if validation is ok 					

			return false;
		}); // /submit product form

	}); // /add product modal btn clicked
	

	// remove product 	

}); // document.ready fucntion

function editInsc(id_insc = null) {

	if(id_insc) {
		$("#id_insc").remove();		
		// remove text-error 
		$(".text-danger").remove();
		// remove from-group error
		$(".form-group").removeClass('has-error').removeClass('has-success');
		// modal spinner
		$('.div-loading').removeClass('div-hide');
		// modal div
		$('.div-result').addClass('div-hide');

		$.ajax({
			url: 'php_action/fetchSelectedInsc.php',
			type: 'post',
			data: {id_insc: id_insc},
			dataType: 'json',
			success:function(response) {		
			// alert(response.product_image);
				// modal spinner
				$('.div-loading').addClass('div-hide');
				// modal div
				$('.div-result').removeClass('div-hide');				

				$(".editInscFooter").append('<input type="hidden" name="id_insc" id="id_insc" value="'+response.id_user+'" />');				
				//$(".editInscPhotoFooter").append('<input type="hidden" name="id_insc" id="id_insc" value="'+response.id_insc+'" />');				
				
				// product name
				$("#editNomInsc").val(response.nom_user);
				// quantity
				$("#editPreInsc").val(response.pre_user);

				$("#editDetInsc").val(response.det_user);
				// rate
				$("#editTelInsc").val(response.tel_user);
				// brand name
				$("#editEmlInsc").val(response.eml_user);
				// category name
				$("#editPwdInsc").val(response.pwd_user);

				$("#editSltFonc").val(response.id_fonc);
				// status
				//$("#editIdRespo").val(response.id);

				// update the product data function
				$("#editInscForm").unbind('submit').bind('submit', function() {

					// form validation
					//var respoImage = $("#editInscImage").val();
					var nom_insc = $("#editNomInsc").val();
					var pre_insc = $("#editPreInsc").val();
					var det_insc = $("#editDetInsc").val();
					var tel_insc = $("#editTelInsc").val();
					var eml_insc = $("#editEmlInsc").val();
					var pwd_insc = $("#editPwdInsc").val();
					var slt_fonc = $("#editSltFonc").val();
					//var productStatus = $("#editProductStatus").val();
								

					if(nom_insc == "") {
						$("#editNomInsc").after('<p class="text-danger">Product Name field is required</p>');
						$('#editNomInsc').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editNomInsc").find('.text-danger').remove();
						// success out for form 
						$("#editNomInsc").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(pre_insc == "") {
						$("#editPreInsc").after('<p class="text-danger">Quantity field is required</p>');
						$('#editPreInsc').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editPreInsc").find('.text-danger').remove();
						// success out for form 
						$("#editPreInsc").closest('.form-group').addClass('has-success');	  	
					}	// /elseInsc

					if(det_insc == "") {
						$("#editDetInsc").after('<p class="text-danger">Quantity field is required</p>');
						$('#editDetInsc').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editDetInsc").find('.text-danger').remove();
						// success out for form 
						$("#editDetInsc").closest('.form-group').addClass('has-success');	  	
					}	// /elseInsc

					if(tel_insc == "") {
						$("#editTelInsc").after('<p class="text-danger">Rate field is required</p>');
						$('#editTelInsc').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editTelInsc").find('.text-danger').remove();
						// success out for form 
						$("#editTelInsc").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(eml_insc == "") {
						$("#editEmlInsc").after('<p class="text-danger">Brand Name field is required</p>');
						$('#editEmlInsc').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editEmlInsc").find('.text-danger').remove();
						// success out for form 
						$("#editEmlInsc").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(pwd_insc == "") {
						$("#editPwdInsc").after('<p class="text-danger">Category Name field is required</p>');
						$('#editPwdInsc').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editPwdInsc").find('.text-danger').remove();
						// success out for form 
						$("#editPwdInsc").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(slt_fonc == "") {
						$("#editSltFonc").after('<p class="text-danger">Category Name field is required</p>');
						$('#editSltFonc').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editSltFonc").find('.text-danger').remove();
						// success out for form 
						$("#editSltFonc").closest('.form-group').addClass('has-success');	  	
					}	// /else
					
					
					if(nom_insc && pre_insc && tel_insc && eml_insc && pwd_insc && slt_fonc && det_insc) {
						// submit loading button
						
						$("#editInscBtn").button('loading');

						var form = $(this);
						//var formData = new FormData(this);

						$.ajax({
							url : form.attr('action'),
							type: form.attr('method'),
							data: form.serialize(),
							dataType: 'json',
							success:function(response) {
								//console.log(response);
								

								if(response.success == true) {
									// submit loading button

									$("#editInscBtn").button('reset');																		

									$("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);
																			
									// shows a successful message after operation
									$('#edit-insc-messages').html('<div class="alert alert-success">'+
				            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
				            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
				          '</div>');

									// remove the mesages
				          $(".alert-success").delay(500).show(10, function() {
										$(this).delay(3000).hide(10, function() {
											$(this).remove();
										});
									}); // /.alert

				          // reload the manage student table
									manageInscTable.ajax.reload(null, true);

									// remove text-error 
									$(".text-danger").remove();
									// remove from-group error
									$(".form-group").removeClass('has-error').removeClass('has-success');

								} // /if response.success
								
							} // /success function
						}); // /ajax function
					}	 // /if validation is ok 					

					return false;
				}); // update the product data function


			} // /success function
		}); // /ajax to fetch product image

				
	} else {
		alert('error please refresh the page');
	}
} // /edit product function

// remove product 
function removeInsc(id_insc = null) {
	if(id_insc) {
		// remove product button clicked
		$("#removeInscBtn").unbind('click').bind('click', function() {
			// loading remove button
			$("#removeInscBtn").button('loading');
			$.ajax({
				url: 'php_action/removeInsc.php',
				type: 'post',
				data: {id_insc: id_insc},
				dataType: 'json',
				success:function(response) {
					// loading remove button
					$("#removeInscBtn").button('reset');
					if(response.success == true) {
						// remove product modal
						$("#removeInscModal").modal('hide');

						// update the product table
						manageInscTable.ajax.reload(null, false);

						// remove success messages
						$(".remove-messages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						// remove the mesages
	          $(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert
					} else {

						// remove success messages
						$(".removeInscMessages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						// remove the mesages
	          $(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert

					} // /error
				} // /success function manageRespoTable
			}); // /ajax fucntion to remove the product
			return false;
		}); // /remove product btn clicked
	} // /if productid
} // /remove product function

function clearForm(oForm) {
	// var frm_elements = oForm.elements;									
	// console.log(frm_elements);
	// 	for(i=0;i<frm_elements.length;i++) {
	// 		field_type = frm_elements[i].type.toLowerCase();									
	// 		switch (field_type) {
	// 	    case "text":
	// 	    case "password":
	// 	    case "textarea":
	// 	    case "hidden":
	// 	    case "select-one":	    
	// 	      frm_elements[i].value = "";
	// 	      break;
	// 	    case "radio":
	// 	    case "checkbox":	    
	// 	      if (frm_elements[i].checked)
	// 	      {
	// 	          frm_elements[i].checked = false;
	// 	      }
	// 	      break;
	// 	    case "file": 
	// 	    	if(frm_elements[i].options) {
	// 	    		frm_elements[i].options= false;
	// 	    	}
	// 	    default:
	// 	        break;
	//     } // /switch
	// 	} // for
}