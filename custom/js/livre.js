var manageLivreTable;

$(document).ready(function() {
	// top nav bar 
	$('#navLivre').addClass('active');
	// manage product data table
	manageLivreTable = $('#manageLivreTable').DataTable({
		'ajax': 'php_action/fetchLivre.php',
		'order': []
	});

	// add product modal btn clicked
	$("#addLivreModalBtn").unbind('click').bind('click', function() {
		// // product form reset
		$("#submitLivreForm")[0].reset();		

		// remove text-error 
		$(".text-danger").remove();
		// remove from-group error
		$(".form-group").removeClass('has-error').removeClass('has-success');

		/*$("#livreImage").fileinput({
	      overwriteInitial: true,
		    maxFileSize: 2500,
		    showClose: false,
		    showCaption: false,
		    browseLabel: '',
		    removeLabel: '',
		    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
		    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
		    removeTitle: 'Cancel or reset changes',
		    elErrorContainer: '#kv-avatar-errors-1',
		    msgErrorClass: 'alert alert-block alert-danger',
		    defaultPreviewContent: '<img src="assests/images/photo_default.png" alt="Profile Image" style="width:100%;">',
		    layoutTemplates: {main2: '{preview} {remove} {browse}'},								    
	  		allowedFileExtensions: ["jpg", "png", "gif", "JPG", "PNG", "GIF"]
			});   */

		// submit product form
		$("#submitLivreForm").unbind('submit').bind('submit', function() {

			// form validation
			//var livreImage = $("#livreImage").val();
			var titLivre = $("#titLivre").val();
			var ediLivre = $("#ediLivre").val();
			var genLivre = $("#genLivre").val();
			var lanLivre = $("#lanLivre").val();
			var qteLivre = $("#qteLivre").val();
			var resLivre = $("#resLivre").val();
			var staLivre = $("#staLivre").val();
	
			/*if(livreImage == "") {
				$("#livreImage").closest('.center-block').after('<p class="text-danger">Product Image field is required</p>');
				$('#livreImage').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#livreImage").find('.text-danger').remove();
				// success out for form 
				$("#livreImage").closest('.form-group').addClass('has-success');	  	
			}	// /else*/

			if(titLivre == "") {
				$("#titLivre").after('<p class="text-danger">Product Name field is required</p>');
				$('#titLivre').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#titLivre").find('.text-danger').remove();
				// success out for form 
				$("#titLivre").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(ediLivre == "") {
				$("#ediLivre").after('<p class="text-danger">Quantity field is required</p>');
				$('#ediLivre').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#ediLivre").find('.text-danger').remove();
				// success out for form 
				$("#ediLivre").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(genLivre == "") {
				$("#genLivre").after('<p class="text-danger">Rate field is required</p>');
				$('#genLivre').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#genLivre").find('.text-danger').remove();
				// success out for form 
				$("#genLivre").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(lanLivre == "") {
				$("#lanLivre").after('<p class="text-danger">Brand Name field is required</p>');
				$('#lanLivre').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#lanLivre").find('.text-danger').remove();
				// success out for form 
				$("#lanLivre").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(qteLivre == "") {
				$("#qteLivre").after('<p class="text-danger">Category Name field is required</p>');
				$('#qteLivre').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#qteLivre").find('.text-danger').remove();
				// success out for form 
				$("#qteLivre").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(resLivre == "") {
				$("#resLivre").after('<p class="text-danger">Product Status field is required</p>');
				$('#resLivre').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#resLivre").find('.text-danger').remove();
				// success out for form 
				$("#resLivre").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(staLivre == "") {
				$("#staLivre").after('<p class="text-danger">Product Status field is required</p>');
				$('#staLivre').closest('.form-group').addClass('has-error');
			}	else {
				// remov error text field
				$("#staLivre").find('.text-danger').remove();
				// success out for form 
				$("#staLivre").closest('.form-group').addClass('has-success');	  	
			}	// /else

			if(titLivre && ediLivre && genLivre && lanLivre && qteLivre && resLivre && staLivre) {
				// submit loading button
				$("#createLivreBtn").button('loading');

				var form = $(this);
				var formData = new FormData(this);

				$.ajax({
					url : form.attr('action'),
					type: form.attr('method'),
					data: formData,
					dataType: 'json',
					cache: false,
					contentType: false,
					processData: false,
					success:function(response) {

						if(response.success == true) {
							// submit loading button
							$("#createLivreBtn").button('reset');
							
							$("#submitLivreForm")[0].reset();

							$("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);
																	
							// shows a successful message after operation
							$('#add-livre-messages').html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

							// remove the mesages
		          $(".alert-success").delay(500).show(10, function() {
								$(this).delay(3000).hide(10, function() {
									$(this).remove();
								});
							}); // /.alert

		          // reload the manage student table
							manageLivreTable.ajax.reload(null, true);

							// remove text-error 
							$(".text-danger").remove();
							// remove from-group error
							$(".form-group").removeClass('has-error').removeClass('has-success');

						} // /if response.success
						
					} // /success function
				}); // /ajax function
			}	 // /if validation is ok 					

			return false;
		}); // /submit product form

	}); // /add product modal btn clicked
	

	// remove product 	

}); // document.ready fucntion

function editLivre(idLivre = null) {

	if(idLivre) {
		$("#idLivre").remove();		
		// remove text-error 
		$(".text-danger").remove();
		// remove from-group error
		$(".form-group").removeClass('has-error').removeClass('has-success');
		// modal spinner
		$('.div-loading').removeClass('div-hide');
		// modal div
		$('.div-result').addClass('div-hide');

		$.ajax({
			url: 'php_action/fetchSelectedLivre.php',
			type: 'post',
			data: {idLivre: idLivre},
			dataType: 'json',
			success:function(response) {		
			// alert(response.product_image);
				// modal spinner
				$('.div-loading').addClass('div-hide');
				// modal div
				$('.div-result').removeClass('div-hide');				

				$("#getLivreImage").attr('src', 'stock/'+response.livre_image);

				$("#editLivreImage").fileinput({		      
				});  

				// $("#editProductImage").fileinput({
		  //     overwriteInitial: true,
			 //    maxFileSize: 2500,
			 //    showClose: false,
			 //    showCaption: false,
			 //    browseLabel: '',
			 //    removeLabel: '',
			 //    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
			 //    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
			 //    removeTitle: 'Cancel or reset changes',
			 //    elErrorContainer: '#kv-avatar-errors-1',
			 //    msgErrorClass: 'alert alert-block alert-danger',
			 //    defaultPreviewContent: '<img src="stock/'+response.product_image+'" alt="Profile Image" style="width:100%;">',
			 //    layoutTemplates: {main2: '{preview} {remove} {browse}'},								    
		  // 		allowedFileExtensions: ["jpg", "png", "gif", "JPG", "PNG", "GIF"]
				// });  

				// product id 
				$(".editLivreFooter").append('<input type="hidden" name="idLivre" id="idLivre" value="'+response.id_livre+'" />');				
				//$(".editLivrePhotoFooter").append('<input type="hidden" name="idLivre" id="idLivre" value="'+response.id_livre+'" />');				
				
				// product name
				$("#editTitLivre").val(response.tit_livre);
				// quantity
				$("#editEdiLivre").val(response.edi_livre);
				// rate
				$("#editGenLivre").val(response.gen_livre);
				// brand name
				$("#editLanLivre").val(response.lan_livre);
				// category name
				$("#editQteLivre").val(response.qte_livre);
				// category name
				$("#editResLivre").val(response.res_livre);
				// status
				$("#editStaLivre").val(response.act_livre);

				// update the product data function
				$("#editLivreForm").unbind('submit').bind('submit', function() {

					// form validation
					var livreImage = $("#editLivreImage").val();
					var titLivre = $("#editTitLivre").val();
					var ediLivre = $("#editEdiLivre").val();
					var genLivre = $("#editGenLivre").val();
					var lanLivre = $("#editLanLivre").val();
					var qteLivre = $("#editQteLivre").val();
					var resLivre = $("#editResLivre").val();
					var staLivre = $("#editStaLivre").val();
								

					if(titLivre == "") {
						$("#editTitLivre").after('<p class="text-danger">Product Name field is required</p>');
						$('#editTitLivre').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editTitLivre").find('.text-danger').remove();
						// success out for form 
						$("#editTitLivre").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(ediLivre == "") {
						$("#editEdiLivre").after('<p class="text-danger">Quantity field is required</p>');
						$('#editEdiLivre').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editEdiLivre").find('.text-danger').remove();
						// success out for form 
						$("#editEdiLivre").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(genLivre == "") {
						$("#editGenLivre").after('<p class="text-danger">Rate field is required</p>');
						$('#editGenLivre').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editGenLivre").find('.text-danger').remove();
						// success out for form 
						$("#editGenLivre").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(lanLivre == "") {
						$("#editLanLivre").after('<p class="text-danger">Brand Name field is required</p>');
						$('#editLanLivre').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editLanLivre").find('.text-danger').remove();
						// success out for form 
						$("#editLanLivre").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(qteLivre == "") {
						$("#editQteLivre").after('<p class="text-danger">Category Name field is required</p>');
						$('#editQteLivre').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editQteLivre").find('.text-danger').remove();
						// success out for form 
						$("#editQteLivre").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(resLivre == "") {
						$("#editResLivre").after('<p class="text-danger">Product Status field is required</p>');
						$('#editResLivre').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editResLivre").find('.text-danger').remove();
						// success out for form 
						$("#editResLivre").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(staLivre == "") {
						$("#editStaLivre").after('<p class="text-danger">Product Status field is required</p>');
						$('#editStaLivre').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editStaLivre").find('.text-danger').remove();
						// success out for form 
						$("#editStaLivre").closest('.form-group').addClass('has-success');	  	
					}						

					if(titLivre && ediLivre && genLivre && lanLivre && qteLivre && resLivre && staLivre) {
						// submit loading button
						$("#editLivreBtn").button('loading');

						var form = $(this);
						var formData = new FormData(this);

						$.ajax({
							url : form.attr('action'),
							type: form.attr('method'),
							data: formData,
							dataType: 'json',
							cache: false,
							contentType: false,
							processData: false,
							success:function(response) {
								console.log(response);
								if(response.success == true) {
									// submit loading button
									$("#editLivreBtn").button('reset');																		

									$("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);
																			
									// shows a successful message after operation
									$('#edit-livre-messages').html('<div class="alert alert-success">'+
				            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
				            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
				          '</div>');

									// remove the mesages
				          $(".alert-success").delay(500).show(10, function() {
										$(this).delay(3000).hide(10, function() {
											$(this).remove();
										});
									}); // /.alert

				          // reload the manage student table
									manageLivreTable.ajax.reload(null, true);

									// remove text-error 
									$(".text-danger").remove();
									// remove from-group error
									$(".form-group").removeClass('has-error').removeClass('has-success');

								} // /if response.success
								
							} // /success function
						}); // /ajax function
					}	 // /if validation is ok 					

					return false;
				}); // update the product data function

				// update the product image				
				$("#updateLivreImageForm").unbind('submit').bind('submit', function() {					
					// form validation
					var livreImage = $("#editLivreImage").val();					
					
					if(livreImage == "") {
						$("#editLivreImage").closest('.center-block').after('<p class="text-danger">Product Image field is required</p>');
						$('#editLivreImage').closest('.form-group').addClass('has-error');
					}	else {
						// remov error text field
						$("#editLivreImage").find('.text-danger').remove();
						// success out for form 
						$("#editLivreImage").closest('.form-group').addClass('has-success');	  	
					}	// /else

					if(livreImage) {
						// submit loading button
						$("#editLivreImageBtn").button('loading');

						var form = $(this);
						var formData = new FormData(this);

						$.ajax({
							url : form.attr('action'),
							type: form.attr('method'),
							data: formData,
							dataType: 'json',
							cache: false,
							contentType: false,
							processData: false,
							success:function(response) {
								
								if(response.success == true) {
									// submit loading button
									$("#editLivreImageBtn").button('reset');																		

									$("html, body, div.modal, div.modal-content, div.modal-body").animate({scrollTop: '0'}, 100);
																			
									// shows a successful message after operation
									$('#edit-livrePhoto-messages').html('<div class="alert alert-success">'+
				            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
				            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
				          '</div>');

									// remove the mesages
				          $(".alert-success").delay(500).show(10, function() {
										$(this).delay(3000).hide(10, function() {
											$(this).remove();
										});
									}); // /.alert

				          // reload the manage student table
									manageLivreTable.ajax.reload(null, true);

									$(".fileinput-remove-button").click();

									$.ajax({
										url: 'php_action/fetchLivreImageUrl.php?i='+idLivre,
										type: 'post',
										success:function(response) {
										$("#getLivreImage").attr('src', response);		
										}
									});																		

									// remove text-error 
									$(".text-danger").remove();
									// remove from-group error
									$(".form-group").removeClass('has-error').removeClass('has-success');

								} // /if response.success
								
							} // /success function
						}); // /ajax function
					}	 // /if validation is ok 					

					return false;
				}); // /update the product image

			} // /success function
		}); // /ajax to fetch product image

				
	} else {
		alert('error please refresh the page');
	}
} // /edit product function

// remove product 
function removeLivre(idLivre = null) {
	if(idLivre) {
		// remove product button clicked
		$("#removeLivreBtn").unbind('click').bind('click', function() {
			// loading remove button
			$("#removeLivreBtn").button('loading');
			$.ajax({
				url: 'php_action/removeLivre.php',
				type: 'post',
				data: {idLivre: idLivre},
				dataType: 'json',
				success:function(response) {
					// loading remove button
					$("#removeLivreBtn").button('reset');
					if(response.success == true) {
						// remove product modal
						$("#removeLivreModal").modal('hide');

						// update the product table
						manageLivreTable.ajax.reload(null, false);

						// remove success messages
						$(".remove-messages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						// remove the mesages
	          $(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert
					} else {

						// remove success messages
						$(".removeLivreMessages").html('<div class="alert alert-success">'+
		            '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
		            '<strong><i class="glyphicon glyphicon-ok-sign"></i></strong> '+ response.messages +
		          '</div>');

						// remove the mesages
	          $(".alert-success").delay(500).show(10, function() {
							$(this).delay(3000).hide(10, function() {
								$(this).remove();
							});
						}); // /.alert

					} // /error
				} // /success function
			}); // /ajax fucntion to remove the product
			return false;
		}); // /remove product btn clicked
	} // /if productid
} // /remove product function

function clearForm(oForm) {
	// var frm_elements = oForm.elements;									
	// console.log(frm_elements);
	// 	for(i=0;i<frm_elements.length;i++) {
	// 		field_type = frm_elements[i].type.toLowerCase();									
	// 		switch (field_type) {
	// 	    case "text":
	// 	    case "password":
	// 	    case "textarea":
	// 	    case "hidden":
	// 	    case "select-one":	    
	// 	      frm_elements[i].value = "";
	// 	      break;
	// 	    case "radio":
	// 	    case "checkbox":	    
	// 	      if (frm_elements[i].checked)
	// 	      {
	// 	          frm_elements[i].checked = false;
	// 	      }
	// 	      break;
	// 	    case "file": 
	// 	    	if(frm_elements[i].options) {
	// 	    		frm_elements[i].options= false;
	// 	    	}
	// 	    default:
	// 	        break;
	//     } // /switch
	// 	} // for
}